using System;
using Rhino;
using Rhino.Commands;
using Rhino.Geometry;
using Rhino.Input;
using Rhino.Input.Custom;
using Rhino.UI;
using Eto.Drawing;
using Eto.Forms;

namespace Condor.Commands
{
    public class CondorCommand : Rhino.Commands.Command
    {
        private static Window g_main_window;

        private Views.CondorForm Form
        {
            get;
            set;
        }

        public override string EnglishName
        {
            get { return "Condor"; }
        }

        protected override Result RunCommand(Rhino.RhinoDoc doc, RunMode mode)
        {
            if (null == Form)
            {
                Form = new Views.CondorForm(EnglishName, doc);
                if (Form.Platform.IsWpf)
                {
                    var handler = new Eto.Wpf.Forms.HwndFormHandler(RhinoApp.MainWindowHandle());
                    Form.Owner = (g_main_window ?? (g_main_window = new Form(handler)));
                }
                Form.Closed += OnFormClosed;
                Form.Show();
            }

            return Result.Success;
           
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            
            Form.Dispose();
            Form = null;
        }

    }
}