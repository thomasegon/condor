﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;
using System.Drawing;
using Condor.Extensions;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public Image<Bgr, byte> FindContours(Image<Bgr, byte> input)
        {
            Contours = new VectorOfVectorOfPoint();
            Image<Bgr, byte> flippedImg = input.Flip(FlipType.Vertical);
            Image<Bgr, Byte> img = new Image<Bgr, byte>(flippedImg.Bitmap);
            Image<Gray, Byte> gimg = img.Convert<Gray, Byte>();

            CvInvoke.FindContours(gimg, Contours, null, RetrType.Tree, ChainApproxMethod.ChainApproxSimple);

            //Needs to be moved such that the result image always is a copy of the original image

            CvInvoke.DrawContours(flippedImg, Contours, -1, new Bgr(System.Drawing.Color.Red).MCvScalar, 2);
            return flippedImg.Flip(FlipType.Vertical);
        }
        public void FindContours(bool flip = false)
        {
            Contours = new VectorOfVectorOfPoint();
            Image<Bgr, Byte> img = ResultImage;
            if (flip)
            {
                Image<Bgr, byte> flippedImg = ResultImage.Flip(FlipType.Vertical);
                img = new Image<Bgr, byte>(flippedImg.Bitmap);
            }

            Image<Gray, Byte> gimg = img.Convert<Gray, Byte>();

            CvInvoke.FindContours(gimg, Contours, null, RetrType.Tree, ChainApproxMethod.ChainApproxSimple);

            //Needs to be moved such that the result image always is a copy of the original image

            CvInvoke.DrawContours(img, Contours, -1, new Bgr(System.Drawing.Color.Red).MCvScalar, 2);
            if(flip)
                img = img.Flip(FlipType.Vertical);
            ResultImage = img;
        }

        public Image<Gray, byte> DrawRhinoContours(Image<Gray, byte> RhinoImage, VectorOfVectorOfPoint Contours)
        {
            List<List<Point>> PointLists = Contours.ToDrawingPoints();

            foreach (List<Point>PointList in PointLists)
            {
                RhinoImage.DrawPolyline(PointList.ToArray(), true, new Gray(0), 3);
                foreach(Point ContourPoint in PointList)
                {
                    RhinoImage.Draw(new CircleF(ContourPoint, 6), new Gray(0),0);
                }
            }
            return RhinoImage;
        }

        public VectorOfPoint FindMaterialContour(int[,] Hierachy, VectorOfVectorOfPoint Contours)
        {
            VectorOfPoint BiggestContour = null;
            List<int> outer = new List<int>();
            for (int i = 0; i < Hierachy.GetLength(0); i++)
            {
                var row = Hierachy.GetRow(i);
                if (row[3] == -1)
                {
                    if (BiggestContour == null || CvInvoke.ContourArea(BiggestContour) < CvInvoke.ContourArea(Contours[i]))
                    {
                        BiggestContour = Contours[i];
                    }
                }
            }

            return BiggestContour;
        }

        public static int[] FindExternalContours(int[,] hierachy)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < hierachy.GetLength(0); i++)
            {
                var row = hierachy.GetRow(i);
                if (row[2] == -1 && row[3] == -1)
                    result.Add(i);
            }
            return result.ToArray();
        }



    }
}
