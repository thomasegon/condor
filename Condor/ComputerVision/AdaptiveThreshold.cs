﻿using System.Drawing;
using Eto;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.IO;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void AdaptiveThreshold(Image<Bgr, byte> img)
        {
            var inputImage = img;
            Image<Gray, byte> imgGray = inputImage.Convert<Gray, byte>();
            Image<Gray, byte> result = imgGray.ThresholdAdaptive(new Gray(AdaptiveThresholdMaxValue), 
                                                                 AdaptiveThresholdType.MeanC, 
                                                                 ThresholdType.Binary, 
                                                                 AdaptiveThresholdBlockSize, 
                                                                 new Gray(AdaptiveThresholdParam1));

            ResultImage = result.Convert<Bgr,byte>();
        }

        
    }

}
