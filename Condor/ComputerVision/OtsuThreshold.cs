﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;
namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void OtsuThreshold()
        {
            VectorOfUMat umat = new VectorOfUMat();
            CvInvoke.Split(ResultImage, umat);
            CvInvoke.Threshold(umat[0], umat[0], 70, 255, ThresholdType.Otsu);
            CvInvoke.Threshold(umat[1], umat[1], 70, 255, ThresholdType.Otsu);
            CvInvoke.Threshold(umat[2], umat[2], 70, 255, ThresholdType.Otsu);
            CvInvoke.Merge(umat, ResultImage);
        }
        public void OtsuThreshold(ref Image<Bgr,byte> Image)
		{
			VectorOfUMat umat = new VectorOfUMat();
			CvInvoke.Split(Image, umat);
			CvInvoke.Threshold(umat[0], umat[0], 70, 255, ThresholdType.Otsu);
			CvInvoke.Threshold(umat[1], umat[1], 70, 255, ThresholdType.Otsu);
			CvInvoke.Threshold(umat[2], umat[2], 70, 255, ThresholdType.Otsu);
			CvInvoke.Merge(umat, Image);
		}
    }
}
