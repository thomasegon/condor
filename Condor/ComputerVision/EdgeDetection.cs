﻿/*
 * methods used to detect edges
 */
using System;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.IO;

namespace Condor.ComputerVision
{

    partial class CVMethods
    {
        //string fileName;
        public Image<Gray, byte> Canny(Image<Bgr, byte> img)
        {
            Image<Bgr, byte> inputImage = img;
            Image<Gray, byte> imgCanny = new Image<Gray, byte>(inputImage.Height, inputImage.Width, new Gray(0));
            imgCanny = inputImage.Canny(Thres, ThresLinking);
            return imgCanny;//OpenCVImageToEto(imgCanny.ToBitmap());
        }

        public Image<Gray, float> Sobel(Image<Bgr, byte> img)
        {
            var inputImage = img;
            Image<Gray, byte> imgGray = inputImage.Convert<Gray, byte>();
            Image<Gray, float> imgSobel = new Image<Gray, float>(inputImage.Height, inputImage.Width, new Gray(0));
            imgSobel = imgGray.Sobel(XOrder, YOrder, ApertureSobel);
            return imgSobel;//OpenCVImageToEto(imgSobel.ToBitmap());
        }

        public Image<Gray, float> Laplace(Image<Bgr, byte> img)
        {
            //The kernel size must be odd and not larger than 31
            var inputImage = img;
            Image<Gray, byte> imgGray = inputImage.Convert<Gray, byte>();
            Image<Gray, float> imgLaplace = new Image<Gray, float>(inputImage.Width, inputImage.Height, new Gray(0));
            imgLaplace = imgGray.Laplace(ApertureLaplace);
            return imgLaplace;//OpenCVImageToEto(imgLaplace.ToBitmap());
        }

       
    }
}
