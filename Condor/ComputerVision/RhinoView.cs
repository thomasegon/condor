﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Condor.Extensions;
using System.Drawing;
using Rhino.Geometry;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;


namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public Image<Bgr, byte> MakeRhinoView(float smooth)
        {
            List<List<Rhino.Geometry.Point3d>> PointList = Contours.ToRhinoPoints();
            List<List<System.Drawing.Point>> ContourList = new List<List<System.Drawing.Point>>();
            Image<Bgr, Byte> RhinoImage = new Image<Bgr, Byte>(ResultImage.Width, ResultImage.Height, new Bgr(Color.White));


            foreach (List<Point3d> Contour in PointList)
            {

                var poly = Point3d.CullDuplicates(Contour, -1);
                var polyline = new Polyline(poly);
                polyline.Smooth(smooth); // 1 is complete smooth and 0 is no smooth.
                var curve = new PolylineCurve(polyline);

                List<System.Drawing.Point> DrawingPointList = polyline.ToList().ToDrawingPoints();

                CvInvoke.Polylines(RhinoImage, DrawingPointList.ToArray(), true, new Bgr(Color.Black).MCvScalar, 3);

                foreach(System.Drawing.Point CenterPoint in DrawingPointList)
                {
                    CvInvoke.Circle(RhinoImage, CenterPoint, 5, new Bgr(Color.Black).MCvScalar, -1);
                }
                    
            }

            return RhinoImage;


        }
    }
}
