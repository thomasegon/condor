﻿using System.Drawing;
using Eto;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.IO;
using Emgu.CV.VideoSurveillance;


namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void BackgroundSubstraction()
        {
            Image<Gray, byte> Foreground = ResultImage.Convert<Gray, byte>();
            Image<Gray, byte> Background = BackgroundImage.Convert<Gray, byte>();
            Foreground = Foreground.AbsDiff(Background);
            Foreground.SmoothBlur(Foreground.Width, Foreground.Height);
            Foreground = Foreground.ThresholdBinary(new Gray(BackgroundSubtractionThreshold), new Gray(255));
            ResultImage = Foreground.Convert<Bgr, byte>();
        }
    }
}
