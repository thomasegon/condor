﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Eto.Forms;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void watershed()
        {
            if(WatershedMarkers != null && WatershedMarkers.Any())
                ResultImage = watershed(ResultImage, WatershedMarkers, WatershedModesList);
        }
        
        public Image<Bgr, byte> watershed(Image<Bgr, byte> image, Point[][] markList, List<int> number)
        {
            var lines = new VectorOfVectorOfPoint(markList);

            var markers = new Image<Gray, byte>(image.Size);
            markers.SetZero();


            //CvInvoke.DrawContours(markers, lines, -1, new MCvScalar(255, 255, 255), 4);
            for (int i = 0; i < markList.Length; i++)
                CvInvoke.Polylines(markers, lines[i], false, new MCvScalar(number[i], number[i], number[i]), 4);


            var mark = markers.Convert<Gray, int>();

            // Watershed using markers on original image
            CvInvoke.Watershed(image, mark);


            // color of result from watershed
            var color = Color.White;

            // Change color
            var res = new Image<Bgr, byte>(image.Size);
            for (int i = 0; i < mark.Rows; i++)
            {
                for (int j = 0; j < mark.Cols; j++)
                {
                    var graycolor = mark[i, j];
                    if (graycolor.Intensity > 0 && graycolor.Intensity < lines.Size + 1)
                    {
                        if (graycolor.Intensity == 1) // change all pixels with 1 to white, i.e. our material.
                        {
                            res.Data[i, j, 0] = color.A;
                            res.Data[i, j, 1] = color.B;
                            res.Data[i, j, 2] = color.G;
                        }

                    }
                    else // background is set to black
                    {
                        res.Data[i, j, 0] = 0;
                        res.Data[i, j, 1] = 0;
                        res.Data[i, j, 2] = 0;
                    }

                }
            }
            return res;
        }
    }


}
