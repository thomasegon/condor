﻿using System;
using System.Collections.Generic;
using Eto.Drawing;
using Eto;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.IO;

namespace Condor.ComputerVision
{
    public partial class CVMethods
    {

        private VectorOfVectorOfPoint _contours;
        private System.Drawing.Point[][] _watershedMarkers;
        private List<int> _watershedModesList;
        private Image<Bgr, byte> _resultImage;
        private Image<Bgr, byte> _originalImage;
        private Image<Bgr, byte> _backgroundimage;
        private int _thres;
        private int _thresLinking;
        private int _xorder;
        private int _yorder;
        private int _apertureSobel;
        private int _apertureLaplace;
        private int _backgroundsubtractionthreshold;
        private int _adaptivethresholdmaxvalue;
        private int _adaptivethresholdblocksize;
        private int _adaptivethresholdparam1;
        private int _blurringiterations;
        private int _otsumaxvalue;
        private int _otsuthresholdvalue;
        private int _binarymaxvalue;
        private int _binarythresholdvalue;
        private int _croppingmargin;

        public System.Drawing.Point[][] WatershedMarkers
        {
            get { return _watershedMarkers; }
            set { _watershedMarkers = value; }
        }

        public List<int> WatershedModesList
        {
            get { return _watershedModesList; }
            set { _watershedModesList = value; }
        }

        public int Thres
        {
            get { return _thres; }
            set { _thres = value; }
        }
        public int ThresLinking
        {
            get { return _thresLinking; }
            set { _thresLinking = value; }
        }
        public int XOrder
        {
            get { return _xorder; }
            set {
                    if (value == 0 || value == 1)
                    {
                        _xorder = value;
                        _yorder = 1 - value;
                    }
                }
        }
        public int YOrder
        {
            get { return _yorder; }
            set {
                    if (value == 0 || value == 1)
                    {
                        _yorder = value;
                        _xorder = 1 - value;
                    }
                }
        }
        public int ApertureSobel
        {
            get { return _apertureSobel; }
            set {
                    if ((0 < value) && ((value % 2) != 0) && (value <= 31))
                        _apertureSobel = value;
                }
        }
        public int ApertureLaplace
        {
            get { return _apertureLaplace; }
            set {
                    if ((0 < value) && ((value % 2) != 0) && (value <= 31))
                        _apertureLaplace = value;
                }
        }
        public int BackgroundSubtractionThreshold
        {
            get { return _backgroundsubtractionthreshold; }
            set { _backgroundsubtractionthreshold = value; }
        }

        public int AdaptiveThresholdMaxValue
        {
            get { return _adaptivethresholdmaxvalue; }
            set { if (value <= 255) { _adaptivethresholdmaxvalue = value; } }
        }

        public int AdaptiveThresholdBlockSize
        {
            get { return _adaptivethresholdblocksize; }
            set { _adaptivethresholdblocksize = value; }
        }

        public int AdaptiveThresholdParam1
        {
            get { return _adaptivethresholdparam1; }
            set { _adaptivethresholdparam1 = value; }
        }

        public int BlurringIterations
        {
            get { return _blurringiterations; }
            set { if (value >= 0 && value <= 10) { _blurringiterations = value; } }
        }


        public VectorOfVectorOfPoint Contours
        {
            get
            {
                return _contours;
            }
            set
            {
                _contours = value;
            }
        }

        public Image<Bgr, byte> ResultImage
        {
            get
            {
                return _resultImage;
            }
            set
            {
                _resultImage = value;
            }
        }

        // originalImage setter and getter
        public Image<Bgr,byte> OriginalImage
        {
            get
            {
                return _originalImage.Copy();
            }
            set
            {
                _originalImage = value.Copy();
                _resultImage = value.Copy();
            }
        }




        public Image<Bgr, byte> BackgroundImage
        {
            get { return _backgroundimage; }
            set { _backgroundimage = value; }
        }

        public int OtsuMaxValue
        {
            get { return _otsumaxvalue; }
            set { if (value <= 255 && value >= 0) { _otsumaxvalue = value; } }
        }

        public int OtsuThresholdValue
        {
            get { return _otsuthresholdvalue; }
            set { if (value <= 255 && value >= 0) { _otsuthresholdvalue = value; } }
        }

        public int BinaryMaxValue
        {
            get { return _binarymaxvalue; }
            set { if (value <= 255 && value >= 0) { _binarymaxvalue = value; } }
        }

        public int BinaryThresholdValue
        {
            get { return _binarythresholdvalue; }
            set { if (value <= 255 && value >= 0) { _binarythresholdvalue = value; } }
        }

        public int CroppingMargin
        {
            get { return _croppingmargin; }
            set { _croppingmargin = value; }
        }

        public CVMethods()
        {
            Thres = 50;
            ThresLinking = 20;
            XOrder = 1;
            YOrder = 0;
            ApertureSobel = 3;
            ApertureLaplace = 1;
            CroppingMargin = 20;
            BinaryThresholdValue = 70;
            BinaryMaxValue = 255;
            OtsuMaxValue = 0;
            OtsuThresholdValue = 0;
            BlurringIterations = 3;
        }


        public Bitmap OpenCVImageToEto(System.Drawing.Bitmap img)
        {
            MemoryStream stream = new MemoryStream();
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
            return new Bitmap(stream.ToArray());
        }

        public Image<Bgr, byte> EtoImageToOpenCV(Bitmap img)
        {
            MemoryStream stream = new MemoryStream();
            img.Save(stream, ImageFormat.Bitmap);
            return new Image<Bgr, byte>(new System.Drawing.Bitmap(stream));
        }

        public Image ProcessImage()
        {
            //Canny
            var cannyEdges = Canny(OriginalImage);
            //Find contours

            var result = cannyEdges;
            return OpenCVImageToEto(result.ToBitmap());

        }
    }
}
