﻿using System.Drawing;
using Eto;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.IO;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void BinaryThreshold()
        {
            Image<Gray, byte> imgGray = ResultImage.Convert<Gray, byte>();
            Image<Gray, byte> result = imgGray.ThresholdBinary(new Gray(BinaryThresholdValue), new Gray(BinaryMaxValue));

            ResultImage = result.Convert<Bgr, byte>();
        }

        public void BinaryThreshold(ref Image<Bgr,byte> image)
        {
            Image<Gray, byte> imgGray = image.Convert<Gray, byte>();
            Image<Gray, byte> result = imgGray.ThresholdBinary(new Gray(BinaryThresholdValue), new Gray(BinaryMaxValue));

            image = result.Convert<Bgr, byte>();
        }

    }

}