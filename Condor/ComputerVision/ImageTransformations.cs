﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Condor.Extensions;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using System.Drawing;
using Emgu.CV;
using Eto.Forms;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {

        // rotates image in the negative direction of the angle from a minAreaRectangle
        public void ImageRotation()
        {
            // Need to only have one contour, otherwise it crash. Other solution might be to flatten the contours list.
            var contours = new VectorOfVectorOfPoint();
            var hierachy = CvInvoke.FindContourTree(ResultImage.Convert<Gray, byte>(), contours, ChainApproxMethod.ChainApproxSimple);
            var points = FindMaterialContour(hierachy, contours);

            var rect = CvInvoke.MinAreaRect(points);
            
            var rotated = ResultImage.Copy().Rotate(-rect.Angle, new Bgr(0, 0, 0), false);
            ResultImage = rotated;
        }

        public Image<Bgr, byte> centerImage(int scaleup = 4)
        {
            var Roi = ResultImage.ROI;
            Mat src = ResultImage.Mat;

            int max = Math.Max(ResultImage.Height, ResultImage.Width); // make sure we have enough space

            var dst = new Image<Bgr, byte>(new Size(max * scaleup, max * scaleup));

            Point center = new Point(dst.Cols / 2, dst.Rows / 2);

            Rectangle centerBox = new Rectangle(center.X - Roi.Width / 2, center.Y - Roi.Height / 2, Roi.Width, Roi.Height);

            var dstroi = dst.ROI;
            dst.ROI = centerBox;

            src.CopyTo(dst);

            dst.ROI = dstroi;

            ResultImage = dst;
            return FindContours(ResultImage);

        }

        public void cropImage()
        {
            try
            {
                // find contours so we can crop to fit them
                var contours = new VectorOfVectorOfPoint();
                var hierachy = CvInvoke.FindContourTree(ResultImage.Convert<Gray, byte>(), contours, ChainApproxMethod.ChainApproxSimple);
                var cropPoints = FindMaterialContour(hierachy, contours);


                var rect = CvInvoke.BoundingRectangle(cropPoints);

                // Sets new top left corner of rectangle. Makes sure we do not go into minus (out of bounds)  
                rect.X = rect.X - CroppingMargin;
                rect.Y = rect.Y - CroppingMargin;

                rect.Width = rect.Width + CroppingMargin * 2;
                rect.Height = rect.Height + CroppingMargin * 2;

                var cropped = new Mat(ResultImage.Mat, rect);
                ResultImage = cropped.ToImage<Bgr, byte>();
            }
            catch
            {
                MessageBox.Show("Could not find the proper contours to crop the image. Try putting Cropping further down the list of action", MessageBoxType.Error);
            }
            
        }
        public void cropImage(ref Image<Bgr,byte> Image)
		{
			try
			{
				// find contours so we can crop to fit them
				var contours = new VectorOfVectorOfPoint();
				var hierachy = CvInvoke.FindContourTree(Image.Convert<Gray, byte>(), contours, ChainApproxMethod.ChainApproxSimple);
				var cropPoints = FindMaterialContour(hierachy, contours);


				var rect = CvInvoke.BoundingRectangle(cropPoints);

				// Sets new top left corner of rectangle. Makes sure we do not go into minus (out of bounds)  
				rect.X = rect.X - CroppingMargin;
				rect.Y = rect.Y - CroppingMargin;

				rect.Width = rect.Width + CroppingMargin * 2;
				rect.Height = rect.Height + CroppingMargin * 2;

				var cropped = new Mat(ResultImage.Mat, rect);
				Image = cropped.ToImage<Bgr, byte>();
			}
			catch
			{
				MessageBox.Show("Could not find the proper contours to crop the image. Try putting Cropping further down the list of action", MessageBoxType.Error);
			}

		}
    }
}
