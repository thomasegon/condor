﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Condor.Extensions;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;
using System.Drawing;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void Homography()
        {
            var orig = ResultImage.Copy();
            centerImage();
            Image<Gray, byte> gimg = ResultImage.Copy().Convert<Gray, byte>();



            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();

            var hierachy = CvInvoke.FindContourTree(gimg, contours, ChainApproxMethod.ChainApproxSimple);
            int[] OuterContours = FindExternalContours(hierachy);



            if (OuterContours.Length == 0 || contours.Size == 1) // no reference
            {
                MessageBox.Show("Cannot find reference object. No homography done");
                ResultImage = orig;
                return;
                //throw new InvalidOperationException("Cannot find reference object");
            }

            List<PointF> sourcepoints = new List<PointF>();
            List<PointF> destinationpointsF = new List<PointF>();
            List<Point> destinationpoints = new List<Point>();
            Matrix<float> homomat = new Matrix<float>(3, 3);
            
            foreach (int OuterContour in OuterContours)
            {
                VectorOfPoint CurrentContour = contours[OuterContour];
                Rectangle ContourRectangle = CvInvoke.BoundingRectangle(CurrentContour);
                CvInvoke.ApproxPolyDP(CurrentContour, CurrentContour, CvInvoke.ArcLength(CurrentContour, true) * 0.05, true);
                var convexHull = CvInvoke.ConvexHull(ConvertVectorOfPointToPointF(CurrentContour).ToArray(), true);
                


                if (convexHull.Length == 4) // need to check if there is appropiate transformation reference.
                {
                    sourcepoints.AddRange(convexHull);

                    destinationpointsF.Add(new PointF(ContourRectangle.X,
                                                      ContourRectangle.Y + ContourRectangle.Height));

                    destinationpointsF.Add(new PointF(ContourRectangle.X + ContourRectangle.Width,
                                                      ContourRectangle.Y + ContourRectangle.Height));

                    destinationpointsF.Add(new PointF(ContourRectangle.X + ContourRectangle.Width,
                                                      ContourRectangle.Y));

                    destinationpointsF.Add(new PointF(ContourRectangle.X,
                                                      ContourRectangle.Y));
                }

            }

            destinationpoints = ConvertPointFListToPointList(destinationpointsF);

            if (sourcepoints.Count < 4) // we cannot do homography with less than 4 points. Check if contours looks alright.
            {
                MessageBox.Show("Contour not good enough to use homography. No homography done", MessageBoxType.Warning);
                ResultImage = orig;
                return;
                //throw new InvalidOperationException("Contour not good enough to use homography");
            }

            var warped = new Image<Gray, byte>(ResultImage.Size);


            CvInvoke.FindHomography(sourcepoints.ToArray(), destinationpointsF.ToArray(), homomat, HomographyMethod.Ransac, 3.0);
            CvInvoke.WarpPerspective(gimg, warped, homomat, warped.Size);
            
            
            ResultImage = warped.Convert<Bgr, byte>();
            cropImage();
        }

        static List<PointF> ConvertVectorOfPointToPointF(VectorOfPoint Vector)
        {
            List<PointF> Result = new List<PointF>();
            for (int i = 0; i < Vector.Size; i++)
            {
                Result.Add(new PointF(Vector[i].X, Vector[i].Y));
            }
            return Result;
        }

        static List<Point> ConvertPointFListToPointList(List<PointF> InputList)
        {
            List<Point> Result = new List<Point>();
            foreach (var FloatPoint in InputList)
            {
                Result.Add(new Point((int)Math.Round(FloatPoint.X), (int)Math.Round(FloatPoint.Y)));
            }
            return Result;
        }
    }
}


