﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV;
using System.Drawing;

namespace Condor.ComputerVision
{
    partial class CVMethods
    {
        public void Blurring()
        {
            for (int i = 1; i < BlurringIterations; i += 2)
            {
                Mat kernelloop = CvInvoke.GetStructuringElement(ElementShape.Ellipse, new Size(i + 1, i + 1), new Point(-1, -1));
                CvInvoke.MorphologyEx(ResultImage, ResultImage, MorphOp.Close, kernelloop, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(255, 0, 0));
                CvInvoke.MorphologyEx(ResultImage, ResultImage, MorphOp.Open, kernelloop, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(255, 0, 0));
            }

            Mat kernelgradient = CvInvoke.GetStructuringElement(ElementShape.Ellipse, new Size(3, 3), new Point(-1, -1));
            CvInvoke.MorphologyEx(ResultImage, ResultImage, MorphOp.Erode, kernelgradient, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(255, 0, 0));
        }

        public void Blurring(ref Image<Bgr, byte> image)
        {
            for (int i = 1; i < BlurringIterations; i += 2)
            {
                Mat kernelloop = CvInvoke.GetStructuringElement(ElementShape.Ellipse, new Size(i + 1, i + 1), new Point(-1, -1));
                CvInvoke.MorphologyEx(image, image, MorphOp.Close, kernelloop, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(255, 0, 0));
                CvInvoke.MorphologyEx(image, image, MorphOp.Open, kernelloop, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(255, 0, 0));
            }

            Mat kernelgradient = CvInvoke.GetStructuringElement(ElementShape.Ellipse, new Size(3, 3), new Point(-1, -1));
            CvInvoke.MorphologyEx(image, image, MorphOp.Erode, kernelgradient, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(255, 0, 0));
        }

    }
}
