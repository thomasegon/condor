﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Util;
using Rhino.Geometry;
using Rhino.DocObjects;

namespace Condor.RhinoMethods
{
    partial class RhinoHandler
    {
        /// <summary>
        /// Adds Contours to the Rhino Document, after Scaling, Moving, rotating and smoothing, the contours such that the contours
        /// are of the correct size and orientation
        /// </summary>
        /// <param name="doc">Rhino Document that the contours are added to</param>
        /// <param name="PointList">List of Contours</param>
        /// <param name="realX">The real size of the object along the X axis</param>
        /// <param name="realY">The real size of the object along the X axis</param>
        /// <param name="smooth">Amount the Contours should be smoothed. Range from 0.0-1.0</param>
        /// <param name="Orientpoints">A List of 2 points that the user have given to scale, move, and rotate the contours </param>
        public void ImportContoursToRhino(Rhino.RhinoDoc doc, List<List<Rhino.Geometry.Point3d>> PointList, int RealDistance, float smooth, List<System.Drawing.Point> Orientpoints)
        {
            string layerName = "Condor";

            //Finds if the Rhino Document contains a Layer with the name Condor
            int layerIndex = doc.Layers.Find(layerName, true);

            //Points that the user has selected from the Export Dialog
            Point3d FromPoint0 = new Point3d(Orientpoints[0].X, Orientpoints[0].Y, 0);
            Point3d FromPoint1 = new Point3d(Orientpoints[1].X, Orientpoints[1].Y, 0);

            // Sets the Layer in Rhino if no Condor layer exists
            if (layerIndex < 0)
            {
                Layer condorLayer = new Layer();
                condorLayer.Name = layerName;
                condorLayer.Color = System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.DarkRed);
                layerIndex = doc.Layers.Add(condorLayer);
            }

            //Sets the attributes for the Condor layer in Rhino
            var attributes = new ObjectAttributes();
            attributes.LayerIndex = layerIndex;

            int ContourCounter = 0;

            // creates the contours
            foreach (List<Point3d> Contour in PointList)
            {
                ContourCounter += 1;
                // cull duplicate points from contours
                var poly = Point3d.CullDuplicates(Contour,-1);
                var polyline = new Polyline(poly);

                //Smooths the Polylines by the user specified amount
                polyline.Smooth(smooth); // 1 is complete smooth and 0 is no smooth.
                var Curve = new PolylineCurve(polyline);

                //Rotates, Moves, and Scales the PolylineCurve such that the scanned material's buttom left corner is situated in the point 0,0,0
                // and makes it so the two points selected by the user sits horizontal with Y=0
                Curve = Rotate(Curve, FromPoint0, FromPoint1);
                Curve = Move(Curve, FromPoint0);
                Curve = Scale(Curve, RealDistance, FromPoint0, FromPoint1);
                
                // Adds the PolylineCurves to the Rhino Document
                doc.Objects.AddCurve(Curve, attributes);
                
                
            }


            doc.Views.Redraw();
            Rhino.RhinoApp.WriteLine("Added object to the document, made from {0} contours", ContourCounter);
        }

        /// <summary>
        /// Moves a polylinecurve to the origin point of Rhino
        /// </summary>
        /// <param name="Contour">Contour that is to be moved</param>
        /// <param name="FromPoint0">Reference point that will end up in the origin point</param>
        /// <returns>Returns the moved Contour to the origin point</returns>
        private PolylineCurve Move(PolylineCurve Contour, Point3d FromPoint0)
        {
            Point3d OriginPoint = new Point3d(0, 0, 0);
            Transform xform_move = Transform.Translation(OriginPoint - FromPoint0);
            Contour.Transform(xform_move);
            return Contour;
        }

        /// <summary>
        /// Scales a polylinecurve based on a scaling a scaling factor described by, the distance between Frompoint0 and FromPoint1, and the RealDistance.
        /// </summary>
        /// <param name="Contour">Contour that is to be scaled</param>
        /// <param name="RealDistance">Is the distance between FromPoint0 and FromPoint1 in the real world</param>
        /// <param name="FromPoint0">First reference point selected by the user</param>
        /// <param name="FromPoint1">Second reference point selected by the user</param>
        /// <returns>Returns the scaled polylinecurve</returns>
        private PolylineCurve Scale(PolylineCurve Contour, double RealDistance, Point3d FromPoint0, Point3d FromPoint1)
        {
            Point3d OriginPoint = new Point3d(0, 0, 0);
            
            double Distance = Math.Sqrt(Math.Pow((FromPoint0.X - FromPoint1.X), 2) + Math.Pow((FromPoint0.Y - FromPoint1.Y), 2));
            double Scale = RealDistance / Distance;

            Transform xform_scale = Transform.Scale(OriginPoint, Scale);

            Contour.Transform(xform_scale);
            return Contour;
        }

        /// <summary>
        /// Rotates a polylinecurve such that the Points FromPoint0 and Frompoint1 has a Y value of 0
        /// </summary>
        /// <param name="Contour">Contour that is to be rotated</param>
        /// <param name="FromPoint0">First refernence point selected by the user</param>
        /// <param name="FromPoint1">Second refernence point selected by the user</param>
        /// <returns>Returns the rotated polylinecurve</returns>
        private PolylineCurve Rotate(PolylineCurve Contour, Point3d FromPoint0, Point3d FromPoint1)
        {
            Point3d ToPoint0 = new Point3d(0, 0, 0);
            Point3d ToPoint1 = new Point3d(1, 0, 0);
            var v0 = FromPoint1 - FromPoint0;
            var v1 = ToPoint1 - ToPoint0;
            Transform xform_rotate = Transform.Rotation(v0, v1, FromPoint0);
            Contour.Transform(xform_rotate);
            return Contour;
        }

        


    }
}
