﻿using System;
using Eto.Forms;
using Eto.Drawing;
using Condor.ComputerVision;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;
using Condor.Extensions;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        void FilePath()
        {
            string url = PicturePathTextBox.Text;
            try
            {
                PicturePathLabel.Visible = false;
                if (url == "0")
                    CaptureCamera(0);
                else if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                {
                    if (Uri.IsWellFormedUriString(url, UriKind.Relative))
                        LoadURLImage("http://" + url);
                    else
                        LoadURLImage(url);
                }
                else
                    LoadFileImage(url);
                RunMethods();

            }
            catch (Exception e)
            {
                PicturePathLabel.Visible = true;
            }

        }
        protected DialogResult OnOpenfile()
        {
            PicturePathLabel.Visible = false;
            BrowseImageDialog.FileName = null;
            BrowseImageDialog.ShowDialog(LoadPictureButton);
            Bitmap file = null;
            try
            {
                file = new Bitmap(BrowseImageDialog.FileName);
                ResultImage = file;
                VisionMethods.OriginalImage = VisionMethods.EtoImageToOpenCV(file);
                PicturePathTextBox.Text = BrowseImageDialog.FileName;
                RunMethods();

                return DialogResult.Ok;
            }
            catch (Exception e)
            {
                return DialogResult.Cancel;
            }


        }
        void CaptureCamera(int camID)
        {
            VideoCapture camera = new VideoCapture(camID);
            Mat frame = new Mat();
            camera.Retrieve(frame, camID);
            var cvImage = frame.ToImage<Bgr, Byte>();
            VisionMethods.OriginalImage = cvImage;
            ResultImage = VisionMethods.OpenCVImageToEto(cvImage.ToBitmap());
            camera.Dispose();
        }
        void LoadURLImage(string imageUrl)
        {
            WebClient client = new WebClient();
            byte[] data = client.DownloadData(imageUrl);
            MemoryStream mem = new MemoryStream(data);
            Bitmap bitmap = new Bitmap(mem);

            ResultImage = bitmap;
            VisionMethods.OriginalImage = VisionMethods.EtoImageToOpenCV(bitmap);
            
        }
        void LoadFileImage(string imageUrl)
        {
            Bitmap bitmap = new Bitmap(imageUrl);

            ResultImage = bitmap;
            VisionMethods.OriginalImage = VisionMethods.EtoImageToOpenCV(bitmap);
        }

        void OnExport(List<System.Drawing.Point> OrientPoints, int Distance)
        {
            if (VisionMethods.Contours.Size > 0)
            {
                OrientPoints = FlipListOfPoints(OrientPoints);
                RhinoMethods.RhinoHandler handler = new RhinoMethods.RhinoHandler();
                handler.ImportContoursToRhino(Doc, FlipVectorOfVectorPoints(VisionMethods.Contours).ToRhinoPoints(true), Distance, SmoothValue, OrientPoints);

            }
        }

        void OnSettingsChange()
        {
            // One of the settings have changed. May need different methods for each type of change
        }

        int OnParameterChange(TextBox textbox)
        {
            int outParameter;
            Int32.TryParse(textbox.Text, out outParameter);
            return outParameter;
            //currentEdgeMethod();
        }

        private void DoubleClick(object sender, MouseEventArgs e, Point position, List<FuncItem> FuncList)
        {
            Range = GetRange((int)e.Location.Y);
            try
            {
                FuncList[Range].RetrieveDialog().ShowModal();
            }
            catch
            { }
        }

        void OnWebcamClick(object sender, EventArgs e)
        {
            try
            {
                CaptureCamera(1);
            }
            catch
            {
                try
                {
                    CaptureCamera(0);
                }
                catch
                {
                    MessageBox.Show("Could not find a secondary or primary webcam. Are you sure a webcam is connected?", MessageBoxType.Error);
                    return;
                }
            }
            VisionMethods.OriginalImage = VisionMethods.EtoImageToOpenCV(new Bitmap(ResultImage));
            RunMethods();
        }
        private void TabChanged(object sender, EventArgs e)
        {
            RunMethods();
        }

        void OnRotateClockwise()
        {
            VisionMethods.OriginalImage = VisionMethods.OriginalImage.Copy().Rotate(90, new Bgr(0, 0, 0), false);
            RunMethods();
            LoadedRhinoImage.Image = VisionMethods.OpenCVImageToEto(VisionMethods.MakeRhinoView(SmoothValue).Bitmap);
            LoadedImage.Image = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
        }
        void OnRotateCounterClockwise()
        {
            VisionMethods.OriginalImage = VisionMethods.OriginalImage.Copy().Rotate(-90, new Bgr(0, 0, 0), false);
            RunMethods();
            LoadedRhinoImage.Image = VisionMethods.OpenCVImageToEto(VisionMethods.MakeRhinoView(SmoothValue).Bitmap);
            LoadedImage.Image = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
            
        }
    }

}
