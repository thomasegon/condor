﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Eto.Forms;
using Eto.Drawing;
using Condor.ComputerVision;
using System.Diagnostics;
using System.ComponentModel;

namespace Condor.Views
{
    partial class CondorForm
    {
        public Thread th;
        private BackgroundWorker backgroundWorker1;
        private int GetRange(int Location)
        {
            if (Location <= 25) { return 0; }
            else { return ((Location - 26) / ListBoxElementSize) + 1; }
        }
        private void IndexSelected(object sender, MouseEventArgs e, PixelLayout layout, ListBox list, Point position, List<FuncItem> myList)
        {
            MovingPosition = new Point(position.X, position.Y + 25);
            var maxX = list.Size.Width + position.X;
            var maxY = list.Size.Height + position.Y;
            if ((int)e.Location.X + MovingPosition.X > position.X && (int)e.Location.X + MovingPosition.X < maxX && (int)e.Location.Y + MovingPosition.Y > position.Y && (int)e.Location.Y + MovingPosition.Y < maxY)
            {
                try
                {
                    Range = GetRange((int)e.Location.Y);
                    MovingItem = myList[Range];
                    MovingLabel.Text = MovingItem.Name;
                    MoveStarted = true;
                    RemoveAtIndex = Range;
                }
                catch
                {
                    MoveStarted = false;
                }
            }
        }
        private void MoveMoving(object sender, MouseEventArgs e, PixelLayout layout, ListBox list, ListBox list2, Point position, Point position2, List<FuncItem> RemoveFrom, List<FuncItem> AddTo)
        {
            var maxX = list2.Size.Width + position2.X;
            var maxY = list2.Size.Height + position2.Y;

            var maxX1 = list.Size.Width + position.X;
            var maxY1 = list.Size.Height + position.Y;
            if (MovingLabel.Text != "")
            {
                if (MoveStarted)
                {
                    layout.Add(MovingLabel, (int)e.Location.X + MovingPosition.X, (int)e.Location.Y + MovingPosition.Y);
                    Moving = true;
                }
                if (Moving)
                {
                    layout.Move(MovingLabel, (int)e.Location.X + MovingPosition.X, (int)e.Location.Y + MovingPosition.Y);
                    if ((int)e.Location.X + MovingPosition.X > position2.X && (int)e.Location.X + MovingPosition.X < maxX && (int)e.Location.Y + MovingPosition.Y > position2.Y && (int)e.Location.Y + MovingPosition.Y < maxY)
                    {

                        try
                        {
                            Range = GetRange((int)e.Location.Y);
                            if (Range != OldRange)
                            {
                                if (OldRange >= 0)
                                {
                                    AddTo.RemoveAt(OldRange);
                                }

                                AddTo.Insert(Range, new FuncItem("  ", null, null));

                            }
                            OldRange = Range;

                        }
                        catch
                        {
                            OldRange = -1;
                        }
                    }
                    else if ((int)e.Location.X + MovingPosition.X > position.X && (int)e.Location.X + MovingPosition.X < maxX1 && (int)e.Location.Y + MovingPosition.Y > position.Y && (int)e.Location.Y + MovingPosition.Y < maxY1)
                    {
                        try
                        {
                            if (RemoveFrom[RemoveAtIndex].Name == MovingLabel.Text)
                            {
                                RemoveFrom.RemoveAt(RemoveAtIndex);
                            }
                            RangeOwn = GetRange((int)e.Location.Y);
                            if (RangeOwn != OldRangeOwn)
                            {
                                if (OldRangeOwn >= 0)
                                {
                                    RemoveFrom.RemoveAt(OldRangeOwn);
                                }
                                try
                                {
                                    RemoveFrom.Insert(RangeOwn, new FuncItem("  ", null, null));
                                }
                                catch
                                {
                                    RangeOwn = -1;
                                }

                            }
                            OldRangeOwn = RangeOwn;

                        }
                        catch
                        {
                            OldRangeOwn = -1;
                        }
                    }
                    DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
                }
            }
            MoveStarted = false;

        }
        private void MoveEnded(object sender, MouseEventArgs e, PixelLayout layout, ListBox list, ListBox list2, Point position, List<FuncItem> RemoveFrom, List<FuncItem> AddTo)
        {
            var maxX = list2.Size.Width + position.X;
            var maxY = list2.Size.Height + position.Y;

            if (Moving && (int)e.Location.X + MovingPosition.X > position.X && (int)e.Location.X + MovingPosition.X < maxX && (int)e.Location.Y + MovingPosition.Y > position.Y && (int)e.Location.Y + MovingPosition.Y < maxY)
            {
                if (OldRangeOwn >= 0)
                {
                    RemoveFrom.RemoveAt(OldRangeOwn);
                    OldRangeOwn = -1;
                }
                if (OldRange >= 0)
                {
                    AddTo.RemoveAt(OldRange);
                    OldRange = -1;
                }
                try
                {
                    AddTo.Insert(Range, MovingItem);
                }
                catch
                {
                    AddTo.Insert(AddTo.Count, MovingItem);
                }
                // rerun all the methods in the list
                RunMethods();
            }
            else if (Moving)
            {
                if (OldRangeOwn >= 0 && OldRangeOwn < RemoveFrom.Count)
                {
                    RemoveFrom.RemoveAt(OldRangeOwn);
                    OldRangeOwn = -1;
                }
                if (OldRange >= 0)
                {
                    AddTo.RemoveAt(OldRange);
                    OldRange = -1;
                }
                try
                {
                    RemoveFrom.Insert(RangeOwn, MovingItem);
                }
                catch
                {
                    RemoveFrom.Insert(RemoveFrom.Count, MovingItem);
                }
                RunMethods();
            }
            MoveStarted = false;
            layout.Remove(MovingLabel);
            MovingLabel.Text = "";
            MovingItem = null;
            Moving = false;
            DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
        }
        private void ShowTooltip(object sender, MouseEventArgs e, Point location, PixelLayout layout, List<FuncItem> FuncList)
		{
            this.backgroundWorker1.CancelAsync();
            ToolTip.Visible = false;
                
            Range = GetRange((int)e.Location.Y);

            if (FuncList.Count > Range && FuncList[Range].Tooltip != null && FuncList[Range].Tooltip != "")
            {
                layout.Move(ToolTip, (int)(e.Location.X + location.X), (int)(e.Location.Y + location.Y + 50));
                ToolTip.Text = FuncList[Range].Tooltip;
                ToolTip.Size = new Size(ToolTip.Text.Length * 7, 20);
                if (!this.backgroundWorker1.IsBusy)
                { this.backgroundWorker1.RunWorkerAsync(2000); }
                else { this.backgroundWorker1.CancelAsync(); }
            }
        }
        private void RemoveLastTextArea(PixelLayout layout)
        {

			this.backgroundWorker1.CancelAsync();
            ToolTip.Visible = false;
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do not access the form's BackgroundWorker reference directly.
            // Instead, use the reference provided by the sender parameter.
            BackgroundWorker bw = sender as BackgroundWorker;


            // Start the time-consuming operation.
            e.Result = TimeConsumingOperation(bw);

            // If the operation was canceled by the user, 
            // set the DoWorkEventArgs.Cancel property to true.
            if (bw.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(
            object sender,
            RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                // The user canceled the operation.
                ToolTip.Visible = false;
            }
            else
            {
                // The operation completed
                ToolTip.Visible = true;
            }
        }

        private int TimeConsumingOperation(BackgroundWorker bw)
        {
            for (int i = 0; i < 100000000; i++)
            {
                if (backgroundWorker1.CancellationPending) { break; }
            }

            return 0;
        }
        #region Mac Version
        private void MoveRight()
        {
            try
            {
                var placeholder = FuncList1[Inactive.SelectedIndex];
                FuncList1.RemoveAt(Inactive.SelectedIndex);
                FuncList2.Add(placeholder);
                DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
                RunMethods();
            }
            catch(ArgumentOutOfRangeException){}
        }
        private void MoveLeft()
        {
            try
            {
                var placeholder = FuncList2[Active.SelectedIndex];
                FuncList2.RemoveAt(Active.SelectedIndex);
                FuncList1.Add(placeholder);
                DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
                RunMethods();
            }
            catch(ArgumentOutOfRangeException){}
        }
        private void MoveUp()
        {
            var placeholder = FuncList2[Active.SelectedIndex];
            FuncList2.RemoveAt(Active.SelectedIndex);
            try
            {
				FuncList2.Insert(Active.SelectedIndex - 1, placeholder);
				Active.SelectedIndex -= 1;
            }
            catch (ArgumentOutOfRangeException) 
            {
                FuncList2.Insert(Active.SelectedIndex, placeholder);
            }
			DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
			RunMethods();

        }
        private void MoveDown()
        {
            var placeholder = FuncList2[Active.SelectedIndex];
            FuncList2.RemoveAt(Active.SelectedIndex);
            try
            {
                FuncList2.Insert(Active.SelectedIndex + 1, placeholder);
                Active.SelectedIndex += 1;
            }
            catch (ArgumentOutOfRangeException)
            {
                FuncList2.Insert(Active.SelectedIndex, placeholder);
            }
            DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
            RunMethods();
        }
		#endregion
		void RunPresets(object sender, EventArgs e,List<PresetItem> Presets,Dictionary<string, FuncItem> AllActions)
		{
            DropDown dropDown = (DropDown)sender;
			List<FuncItem> active;
			List<FuncItem> inactive;
            List<List<FuncItem>> Result = Presets[dropDown.SelectedIndex].Run(AllActions);
            FuncList1 = Result[0];
            FuncList2 = Result[1];
			DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
			RunMethods();
		}
    }
}