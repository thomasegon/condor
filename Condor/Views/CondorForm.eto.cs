﻿﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using Condor.ComputerVision;

namespace Condor.Views
{

    public partial class CondorForm : Form
    {
        #region Int Constructors
        private int _materialwidth;
        private int _materiallength;
        private int _materialthickness;
        private float _smoothValue;
        private int _range;
        private int _oldRange;
        private int _rangeOwn;
        private int _oldRangeOwn;
        private int _removeAtIndex;
        private int _listBoxElementSize;
        #endregion
        #region Bool Constructors
        private bool _moveStarted;
        private bool _moving;
        #endregion
        #region String Constructors
        private string _name;
        #endregion
        #region Color Constructors
        private Color _materialColorWatershed = Color.FromArgb(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Green).ToArgb());
        private Color _contourColorWatershed = Color.FromArgb(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Red).ToArgb());
        private Color _backgroundColorWatershed = Color.FromArgb(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.DarkOrange).ToArgb());
        #endregion
        #region OpenFileDialog Constructors
        private OpenFileDialog _browseimagedialog;
        #endregion
        #region CVMethods Constructors
        private CVMethods _visionmethods;
        #endregion
        #region Rhino.Doc Constructors
        private Rhino.RhinoDoc _doc;
        #endregion
        #region Image Constructors
        private Image _originalimage;
        private Image _resultimage;
        #endregion
        #region Textbox Constructors
        private TextBox _picturepathtextbox;
		private TextBox _smoothingTextBox;
		private TextBox _smoothingTextBoxAdvanced;
        #endregion
        #region Label Constructors
        private Label _picturepathlabel;
        private Label _messagelabel;
        private Label _movingLabel;
        private Label _inactiveLabel;
        private Label _activeLabel;
        private Label _loadedRhinoImageLabel;
		private Label _loadedImageLabel;
		private Label _smoothingLabel;
		private Label _smoothingLabelAdvanced;
        private Label _homographyLabel;
        #endregion
        #region Button Constructors
        private Button _exportbutton;
        private Button _loadpicturebutton;
        private Button _webcambutton;
        private Button _moveLeftButton;
        private Button _moveRightButton;
        private Button _moveUpButton;
        private Button _moveDownButton;
        private Button _rotateClockwise;
        private Button _rotateCounterClockwise;
        #endregion
        #region ImageView Constructors
        private ImageView _loadedimage;
        private ImageView _loadedrhinoimage;
		#endregion
		#region Point Constructors
		private Point _movingPosition;
        private Point _list1Position;
        private Point _list2Position;
		#endregion
		#region List Constructors
		private List<FuncItem> _funcList1;
		private List<FuncItem> _funcList2;
        private List<PresetItem> _presetList;
        #endregion
        #region FuncItem Constructors
        FuncItem _funcItem;
        #endregion
        #region ListBox Constructors
        private ListBox _inactive;
        private ListBox _active;
        #endregion
        #region Columns Constructors
        private int _col1;
        private int _col2;
        private int _col3;
        private int _col4;
        private int _col5;
        private int _col6;
        #endregion
        #region Rows Constructors
        private int _row1;
        private int _row2;
        private int _row3;
        private int _row4;
		private int _row5;
		private int _row6;
		private int _row7;
        #endregion
        #region TabControl Constructors
        private TabControl _tabs;
        #endregion
        #region TabPage Constructors
        private TabPage _basicTabPage;
        private TabPage _advanceTabPage;
        #endregion
        #region PixelLayout Constructors
        private PixelLayout _basicTab;
        private PixelLayout _advancePage;
		#endregion
		#region Slider Constructors
		private Slider _smoothingSlider;
		private Slider _smoothingSliderAdvanced;
        #endregion
        #region CheckBox Constructors
        private CheckBox _homographyCheckBox;
        #endregion
        #region Size Constructors
        private Size _tabPageSize;
		#endregion
		private TextArea _tooltip;
        private Dictionary<string, FuncItem> _allActions;

		#region Int Accessors
		public int MaterialWidth
        {
            get { return _materialwidth; }
            set { if (value >= 0) { _materialwidth = value; }; }
        }
        public int MaterialLength
        {
            get { return _materiallength; }
            set { if (value >= 0) { _materiallength = value; }; }
        }
        public int MaterialThickness
        {
            get { return _materialthickness; }
            set { if (value >= 0) { _materialthickness = value; }; }
        }
        public int ListBoxElementSize
        {
            get { return _listBoxElementSize; }
            set { _listBoxElementSize = value; }
        }
        // should be between 0 and 1
        public float SmoothValue
        {
            get { return _smoothValue; }
            set
            { 
                if (value < 0)
                    this._smoothValue = 0;
                else if (value > 1)
                    this._smoothValue = 1;
                else
                    this._smoothValue = value;
            }
        }
        public int Range
        {
            get { return _range; }
            set { _range = value; }
        }
		public int OldRange
		{
            get { return _oldRange; }
            set { _oldRange = value; }
		}
		public int RangeOwn
		{
            get { return _rangeOwn; }
            set { _rangeOwn = value; }
		}
		public int OldRangeOwn
		{
            get { return _oldRangeOwn; }
            set { _oldRangeOwn = value; }
		}
		public int RemoveAtIndex
		{
            get { return _removeAtIndex; }
            set { _removeAtIndex = value; }
		}
        #endregion
        #region bool Accessors
        public bool MoveStarted
        {
            get { return _moveStarted; }
            set { _moveStarted = value; }
        }
        public bool Moving
        {
            get { return _moving; }
            set { _moving = value; }
        }
        #endregion
        #region String Accessors
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        #endregion
        #region Color Accessors
        public Color MaterialColorWatershed
        {
            get { return _materialColorWatershed; }
            set { _materialColorWatershed = value; }
        }
        public Color ContourColorWatershed
        {
            get { return _contourColorWatershed; }
            set { _contourColorWatershed = value; }
        }
        public Color BackgroundColorWatershed
        {
            get { return _backgroundColorWatershed; }
            set { _backgroundColorWatershed = value; }
        }
        #endregion
        #region OpenFileDialog Accessor
        public OpenFileDialog BrowseImageDialog
        {
            get { return _browseimagedialog; }
            set { _browseimagedialog = value; }
        }
        #endregion
        #region CVMethod Accessors
        public CVMethods VisionMethods
        {
            get { return _visionmethods; }
            set { _visionmethods = value; }
        }
        #endregion
        #region Rhino.Doc Accessor
        public Rhino.RhinoDoc Doc
        {
            get { return _doc; }
            set { _doc = value; }
        }
        #endregion
        #region Image Accessors
        public Image OriginalImage
        {
            get { return _originalimage; }
            set { _originalimage = value; }
        }
        public Image ResultImage
        {
            get { return _resultimage; }
            set { _resultimage = value; }
        }
        #endregion
        #region TextBox Accessors
        public TextBox PicturePathTextBox
        {
            get { return _picturepathtextbox; }
            set { _picturepathtextbox = value; }
        }
		public TextBox SmoothingTextBox
		{
			get { return _smoothingTextBox; }
			set { _smoothingTextBox = value; }
		}
		public TextBox SmoothingTextBoxAdvanced
		{
			get { return _smoothingTextBoxAdvanced; }
			set { _smoothingTextBoxAdvanced = value; }
		}
        #endregion
        #region Label Accessors
        public Label PicturePathLabel
        {
            get { return _picturepathlabel; }
            set { _picturepathlabel = value; }
        }
        public Label MessageLabel
        {
            get { return _messagelabel; }
            set { _messagelabel = value; }
        }
        public Label MovingLabel
        {
            get { return _movingLabel; }
            set { _movingLabel = value; }
        }
        public Label InactiveLabel
        {
            get { return _inactiveLabel; }
            set { _inactiveLabel = value; }
        }
        public Label ActiveLabel
        {
            get { return _activeLabel; }
            set { _activeLabel = value; }
        }
        public Label LoadedImageLabel
        {
            get { return _loadedImageLabel; }
            set { _loadedImageLabel = value; }
        }
        public Label LoadedRhinoImageLabel
        {
            get { return _loadedRhinoImageLabel; }
            set { _loadedRhinoImageLabel = value; }
        }
		public Label SmoothingLabel
		{
			get { return _smoothingLabel; }
			set { _smoothingLabel = value; }
		}
		public Label SmoothingLabelAdvanced
		{
			get { return _smoothingLabelAdvanced; }
			set { _smoothingLabelAdvanced = value; }
		}
        public Label HomographyLabel
        {
            get { return _homographyLabel; }
            set { _homographyLabel = value; }
        }
        #endregion
        #region Button Accessors
        public Button ExportButton
        {
            get { return _exportbutton; }
            set { _exportbutton = value; }
        }
        public Button LoadPictureButton
        {
            get { return _loadpicturebutton; }
            set { _loadpicturebutton = value; }
        }

        public Button WebCamButton
        {
            get { return _webcambutton; }
            set { _webcambutton = value; }
        }
        public Button MoveLeftButton
        {
            get { return _moveLeftButton; }
            set { _moveLeftButton = value; }
        }
        public Button MoveRightButton
        {
            get { return _moveRightButton; }
            set { _moveRightButton = value; }
        }
        public Button MoveUpButton
        {
            get { return _moveUpButton; }
            set { _moveUpButton = value; }
        }
        public Button MoveDownButton
        {
            get { return _moveDownButton; }
            set { _moveDownButton = value; }
        }

        public Button RotateClockwise
        {
            get { return _rotateClockwise; }
            set { _rotateClockwise = value; }
        }

        public Button RotateCounterClockwise
        {
            get { return _rotateCounterClockwise; }
            set { _rotateCounterClockwise = value; }
        }
        #endregion
        #region imageView accessors
        public ImageView LoadedImage
        {
            get { return _loadedimage; }
            set { _loadedimage = value; }
        }
        public ImageView LoadedRhinoImage
        {
            get { return _loadedrhinoimage; }
            set { _loadedrhinoimage = value; }
        }
        #endregion
        #region Point Accessors
        public Point MovingPosition
        {
            get { return _movingPosition; }
            set { _movingPosition = value; }
        }
        public Point List1Position
        {
            get { return _list1Position; }
            set { _list1Position = value; }
        }
        public Point List2Position
        {
            get { return _list2Position; }
            set { _list2Position = value; }
        }
        #endregion
        #region List Accessors
        public List<FuncItem> FuncList1
        {
            get { return _funcList1; }
            set { _funcList1 = value; }
        }
        public List<FuncItem> FuncList2
        {
            get { return _funcList2; }
            set { _funcList2 = value; }
        }
        public List<PresetItem> PresetList
        {
            get { return _presetList; }
            set { _presetList = value; }
        }
        #endregion
        #region FuncItem Accessors
        public FuncItem MovingItem
        {
            get { return _funcItem; }
            set { _funcItem = value; }
        }
        #endregion
        #region ListBox Accessors
        public ListBox Inactive
        {
            get { return _inactive; }
            set { _inactive = value; }
        }
        public ListBox Active
        {
            get { return _active; }
            set { _active = value; }
        }
        #endregion
        #region Columns Accessors
        public int Col1
        {
            get { return _col1; }
            set { _col1 = value; }
        }
        public int Col2
        {
            get { return _col2; }
            set { _col2 = value; }
        }
        public int Col3
        {
            get { return _col3; }
            set { _col3 = value; }
        }
        public int Col4
        {
            get { return _col4; }
            set { _col4 = value; }
        }
        public int Col5
        {
            get { return _col5; }
            set { _col5 = value; }
        }
        public int Col6
        {
            get { return _col6; }
            set { _col6 = value; }
        }
        #endregion
        #region Rows Accessors
        public int Row1
        {
            get { return _row1; }
            set { _row1 = value; }
        }
        public int Row2
        {
            get { return _row2; }
            set { _row2 = value; }
        }
        public int Row3
        {
            get { return _row3; }
            set { _row3 = value; }
        }
        public int Row4
        {
            get { return _row4; }
            set { _row4 = value; }
        }
        public int Row5
        {
            get { return _row5; }
            set { _row5 = value; }
        }
		public int Row6
		{
			get { return _row6; }
			set { _row6 = value; }
		}
		public int Row7
		{
			get { return _row7; }
			set { _row7 = value; }
		}
        #endregion
        #region TabControl Accessors
        public TabControl Tabs
        {
            get { return _tabs; }
            set { _tabs = value; }
        }
        #endregion
        #region TabPage Accessors
        public TabPage BasicTabPage
        {
            get { return _basicTabPage; }
            set { _basicTabPage = value; }
        }
        public TabPage AdvanceTabPage
        {
            get { return _advanceTabPage; }
            set { _advanceTabPage = value; }
        }
        #endregion
        #region PixelLayout Accessors
        public PixelLayout BasicPage
        {
            get { return _basicTab; }
            set { _basicTab = value; }
        }
        public PixelLayout AdvancePage
        {
            get { return _advancePage; }
            set { _advancePage = value; }
        }
		#endregion
		#region Slider Accessors
		public Slider SmoothingSlider
		{
			get { return _smoothingSlider; }
			set { _smoothingSlider = value; }
		}
		public Slider SmoothingSliderAdvanced
		{
			get { return _smoothingSliderAdvanced; }
			set { _smoothingSliderAdvanced = value; }
		}
        #endregion
        #region CheckBox Accessors
        public CheckBox HomographyCheckBox
        {
            get { return _homographyCheckBox; }
            set { _homographyCheckBox = value; }
        }
        #endregion
        #region Size Accessors
        public Size TabPageSize
        {
            get { return _tabPageSize; }
            set { _tabPageSize = value; }
        }
        #endregion
        public TextArea ToolTip
        {
            get { return _tooltip; }
            set { _tooltip = value; }
        }
        public Dictionary<string, FuncItem> AllActions
        {
            get { return _allActions; }
            set { _allActions = value; }
        }


		public CondorForm(string name, Rhino.RhinoDoc document)
        {
            MaterialThickness = 0;
            MaterialWidth = 0;
            MaterialLength = 0;
            SmoothValue = 0;
            MoveStarted = false;
            Moving = false;
            Range = 0;
            OldRange = -1;
            RangeOwn = 0;
            OldRangeOwn = -1;
            RemoveAtIndex = -1;
            ListBoxElementSize = 23;
            if (Platform.IsMac)
            {
                ListBoxElementSize = 19;
            }
            TabPageSize = new Size(365, 365);


            BrowseImageDialog = new OpenFileDialog();
            VisionMethods = new CVMethods();

            //new List<FuncItem> { new FuncItem("method1", print, new Dialog()), new FuncItem("method2", print, new Dialog()), new FuncItem("method3", print, new Dialog()), new FuncItem("method4", print, new Dialog()) };

            List<FuncItem> active = new List<FuncItem>();
            List<FuncItem> inactive = new List<FuncItem>();
			AllActions = new Dictionary<string, FuncItem>()
			{
				{"Blurring", new FuncItem("Blurring", VisionMethods.Blurring, BlurringDialog, "Blurring tooltip")},
                {"Boundary Enchancement",new FuncItem("Boundary Enchancement", VisionMethods.OtsuThreshold, OtsuDialog, "Boundary Enchancement tooltip")},
                //{"Homography", new FuncItem("Homography", VisionMethods.Homography, NotImplemetedDialog, "Homography tooltip")},
                {"Object Selection", new FuncItem("Object Selection", VisionMethods.watershed, SegmentationDialog, "Object Selection tooltip")},
                {"Cropping", new FuncItem("Cropping", VisionMethods.cropImage, CroppingDialog, "Cropping tooltip")},
                {"Binary Threshold", new FuncItem("Binary Threshold", VisionMethods.BinaryThreshold, BinaryDialog, "Binary Threshold tooltip")}
            };
            DefaultMethodSetup(out active, out inactive);
            FuncList1 = inactive;
            FuncList2 = active;

            Name = name;
            Doc = document;

            #region ROWS
            Row1 = 0;
            Row2 = 25;
            Row3 = 230;
            Row4 = 255;
            Row5 = 280;
            Row6 = 305;
            Row7 = 330;
            #endregion            
            #region COLS
            Col1 = 5;
            Col2 = 200;
            Col3 = 400;
            Col4 = 600;
            Col5 = 800;
            Col6 = 1000;
            #endregion
            #region TextBox
            PicturePathTextBox = new TextBox
            {
                BackgroundColor = new Color(50, 50, 50),
                MaxLength = 1000,
                PlaceholderText = "Path of the picture or url to picture",
                Size = new Size(720,25)
            };
			SmoothingTextBox = new TextBox
			{
				MaxLength = 100,
				Size = new Size(150, 25),
				Text = SmoothValue.ToString()
			};
			SmoothingTextBoxAdvanced = new TextBox
			{
				MaxLength = 100,
				Size = new Size(150, 25),
				Text = SmoothValue.ToString()
			};
            #endregion
            #region Labels
            PicturePathLabel = new Label { Text = "Invalid URL", Visible = false};
            MessageLabel = new Label { Text = "", Size = new Size(400, 75) };
            MovingLabel = new Label();
            InactiveLabel = new Label { Text = "Inactive actions", Size = new Size(150, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
            ActiveLabel = new Label { Text = "Active actions", Size = new Size(150, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
            LoadedImageLabel = new Label { Text = "Loaded image", Size = new Size(400, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
            LoadedRhinoImageLabel = new Label { Text = "Rhino image", Size = new Size(400, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
			SmoothingLabel = new Label { Text = "Smooth", Size = new Size(TabPageSize.Width, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
			SmoothingLabelAdvanced = new Label { Text = "Smooth", Size = new Size(TabPageSize.Width, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
			HomographyLabel = new Label { Text = "Homography", Size = new Size(TabPageSize.Width, 25), TextAlignment = TextAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
            #endregion
            #region Buttons
            ExportButton = new Button { Text = "Export", Size = new Size(150, 25) };
            LoadPictureButton = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.open_folder), Size = new Size(30, 30) };
            WebCamButton = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.webcamImage), Size = new Size(30,30)};
            MoveUpButton = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.up_arrow), Size = new Size(40,40) };
            MoveDownButton = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.angle_arrow_down), Size = new Size(40, 40) };
            MoveLeftButton = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.angle_pointing_to_left), Size = new Size(40, 40) };
            MoveRightButton = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.angle_arrow_pointing_to_right), Size = new Size(40, 40) };
            RotateClockwise = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.refresh_arrow), Size = new Size(25,25) };
            RotateCounterClockwise = new Button { Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.undo_arrow), Size = new Size(25, 25) };
            #endregion
            #region ImageView
            LoadedImage = new ImageView { Size = new Size(400, 200) };
            LoadedRhinoImage = new ImageView { Size = new Size(400, 200) };
            #endregion
            #region ListBox
            var ListBoxSize = new Size(120, 250);
            Inactive = new ListBox { Size = ListBoxSize };
            Inactive.BindDataContext(c => c.DataStore, (List<List<FuncItem>> m) => m[0]);
            Active = new ListBox { Size = ListBoxSize };
            Active.BindDataContext(c => c.DataStore, (List<List<FuncItem>> m) => m[1]);
			#endregion
			#region Slider
			SmoothingSlider = new Slider { MinValue = 0, MaxValue = 100, Value = (int)(SmoothValue * 100), Width = 175, SnapToTick = true };
			SmoothingSliderAdvanced = new Slider { MinValue = 0, MaxValue = 100, Value = (int)(SmoothValue * 100), Width = 175, SnapToTick = true };
			#endregion
			#region CheckBox
			HomographyCheckBox = new CheckBox {};
            #endregion
            #region Button Events
            LoadPictureButton.Click += (sender, e) => OnOpenfile();
            ExportButton.Click += (sender, e) => ExportDialog().ShowModal();
            WebCamButton.Click += (sender, e) => OnWebcamClick(sender, e);
            RotateClockwise.Click += (sender, e) => OnRotateClockwise();
            RotateCounterClockwise.Click += (sender, e) => OnRotateCounterClockwise();
            #endregion
            #region Textbox Events
            PicturePathTextBox.KeyDown += (sender, e) => { if (e.IsKeyDown(Keys.Enter)) { FilePath(); } };
			#endregion
			#region Smoothing Events
			SmoothingSlider.ValueChanged += (sender, e) => SmoothSliderChange(SmoothingSlider, SmoothingTextBox);
            SmoothingSlider.MouseUp += (sender, e) => RunMethods();
			SmoothingTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            SmoothingTextBox.TextChanged += (sender, e) => SmoothingTextBoxChange(SmoothingTextBox, SmoothingSlider);
			SmoothingSliderAdvanced.ValueChanged += (sender, e) => SmoothSliderChange(SmoothingSliderAdvanced, SmoothingTextBoxAdvanced);
            SmoothingSliderAdvanced.MouseUp += (sender, e) => RunMethods();
			SmoothingTextBoxAdvanced.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            SmoothingTextBoxAdvanced.TextChanged += (sender, e) => SmoothingTextBoxChange(SmoothingTextBoxAdvanced, SmoothingSliderAdvanced);
			#endregion
			#region Tooltips
			WebCamButton.ToolTip = "Use webcam to take picture";
            LoadPictureButton.ToolTip = "Choose picture from computer";
            PicturePathTextBox.ToolTip = "Write URL to website picture or path of picture on computer";
            ExportButton.ToolTip = "Export contours to Rhino3D";
            LoadedImage.ToolTip = "Image with contours";
            LoadedRhinoImage.ToolTip = "Contours that will be loaded into Rhino3D";
            RotateClockwise.ToolTip = "Rotates picture clockwise";
            RotateCounterClockwise.ToolTip = "Rotates picture counter clockwise";
            MoveRightButton.ToolTip = "Choose element in Inactive list and click the button to move element to Active list";
            MoveLeftButton.ToolTip = "Choose element in Active list and click the button to move element to Inactive list";
            MoveUpButton.ToolTip = "Choose element in Active list and click the button to move element up";
            MoveDownButton.ToolTip = "Choose element in Active list and click the button to move element down";
            #endregion
            ToolTip = new TextArea { Text = "Hest", BackgroundColor = Color.FromGrayscale(10), Size = new Size(100, 25),Visible=false };

            #region CheckBox Events
            HomographyCheckBox.CheckedChanged += (sender, e) => RunMethods();
			#endregion

			#region Tabs Control Layout
			#region BasicTab
			BasicTabPage = new TabPage { Text = "Basic" };
            BasicPage = new PixelLayout { Size = TabPageSize };
            //BasicPage.Add(HomographyLabel, 0, 0);
            //BasicPage.Add(HomographyCheckBox, 180, 25);
			BasicPage.Add(SmoothingLabel, 0, Inactive.Height + 50);
			BasicPage.Add(SmoothingSlider, 0, Inactive.Height + 75);
			BasicPage.Add(SmoothingTextBox, 188, Inactive.Height + 75);

            BasicTabPage.Content = BasicPage;
            #endregion

            #region AdvanceTab
            List<PresetItem> Presets = new List<PresetItem>()
			{
				new PresetItem("Default",new List<FuncItem>(){AllActions["Blurring"], AllActions["Boundary Enchancement"]}),
                new PresetItem("Empty",new List<FuncItem>(){})
            };
            DropDown PresetDropDown = new DropDown();
            foreach(var item in Presets)
            {
                PresetDropDown.Items.Add(item.ToString());
            }
            PresetDropDown.SelectedIndexChanged += (sender, e) => RunPresets(sender, e, Presets, AllActions);
            AdvanceTabPage = new TabPage { Text = "Advanced" };
            AdvancePage = new PixelLayout { Size = TabPageSize };
            AdvancePage.Add(new Label(){Text = "Presets"},Col1, 0);
            AdvancePage.Add(PresetDropDown, Col2, 0);

            AdvancePage.Add(InactiveLabel, Col1, Row1 +25);

            List1Position = new Point(Col1, Row2+ 25);
            List2Position = new Point(Col2, Row2+ 25);
            AdvancePage.Add(Inactive, List1Position);

            AdvancePage.Add(ActiveLabel, Col2, Row1+ 25);
            AdvancePage.Add(Active, List2Position);

            AdvancePage.Add(SmoothingLabelAdvanced, 0,  Inactive.Height + 25+ 25);
			AdvancePage.Add(SmoothingSliderAdvanced, 0, Inactive.Height + 50+ 25);
			AdvancePage.Add(SmoothingTextBoxAdvanced, 188, Inactive.Height + 50+ 25);

            AdvancePage.Add(MoveRightButton, Col1 + Inactive.Width + 15, 125);
            AdvancePage.Add(MoveLeftButton, Col1 + Inactive.Width + 15, 175);
            AdvancePage.Add(MoveUpButton, Col2 + Active.Width + 2, 125);
            AdvancePage.Add(MoveDownButton, Col2 + Active.Width + 2, 175);

            AdvanceTabPage.Content = AdvancePage;
            #endregion

            Tabs = new TabControl { Pages = { BasicTabPage, AdvanceTabPage } };
            Tabs.SelectedIndexChanged += (sender, e) => TabChanged(sender, e);
            #endregion

            InitializeComponent();
        }


        void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            Title = "Condor";
            BackgroundColor = new Color(1, 1, 1);
            DataContext = new List<List<FuncItem>> { FuncList1, FuncList2 };
            Maximizable = true;
            Minimizable = true;
            
       
            #region Layout
            var layout = new PixelLayout { Size = new Size(1210, 410) };

            layout.Add(Tabs, Col1, Row1);

            layout.Add(LoadedImageLabel, Col3, Row1);
            layout.Add(LoadedImage,Col3,Row2);

            layout.Add(LoadedRhinoImageLabel, Col5, Row1);
            layout.Add(LoadedRhinoImage,Col5,Row2);


            layout.Add(RotateCounterClockwise, Col4-25, Row3);
            layout.Add(RotateClockwise, Col4, Row3);

            layout.Add(WebCamButton, Col3, Row4);

            layout.Add(LoadPictureButton, Col3 + 35, Row4);
            layout.Add(PicturePathTextBox, Col3 + 75, Row4);


            layout.Add(ToolTip, Col5, Row5);
            //layout.Add(MessageLabel, Col5, Row5);

            layout.Add(ExportButton, Col6+50,Row7+50);

            Content = layout;
			#endregion
			MoveRightButton.Click += (sender, e) => MoveRight();
			MoveLeftButton.Click += (sender, e) => MoveLeft();
			MoveUpButton.Click += (sender, e) => MoveUp();
			MoveDownButton.Click += (sender, e) => MoveDown();
            Inactive.MouseMove += (sender, e) => ShowTooltip(sender, e, List1Position, layout, FuncList1);
			Inactive.MouseLeave += (sender, e) => RemoveLastTextArea(layout);
            Active.MouseMove += (sender, e) => ShowTooltip(sender, e, List2Position, layout, FuncList2);
			Active.MouseLeave += (sender, e) => RemoveLastTextArea(layout);
            if(Platform.IsMac)
            {
                Inactive.MouseDown += (sender, e) => { if (e.Buttons == (MouseButtons.Alternate)) { DoubleClick(sender, e, List2Position, FuncList1); } };
                Inactive.MouseDoubleClick += (sender, e) => DoubleClick(sender, e, List1Position, FuncList1);
                Active.MouseDown += (sender, e) =>{ if (e.Buttons == (MouseButtons.Alternate)) { DoubleClick(sender, e, List2Position, FuncList2); }};
                Active.MouseDoubleClick += (sender, e) => DoubleClick(sender, e, List2Position, FuncList2);
            }
            else
            {
				Inactive.MouseDown += (sender, e) => { if (e.Buttons == (MouseButtons.Alternate)) { DoubleClick(sender, e, List2Position, FuncList1); } else { IndexSelected(sender, e, layout, Inactive, List1Position, FuncList1); } };
				Inactive.MouseMove += (sender, e) => MoveMoving(sender, e, layout, Inactive, Active, List1Position, List2Position, FuncList1, FuncList2);
				Inactive.MouseUp += (sender, e) => MoveEnded(sender, e, layout, Inactive, Active, List2Position, FuncList1, FuncList2);
				Inactive.MouseDoubleClick += (sender, e) => DoubleClick(sender, e, List1Position, FuncList1);

				Active.MouseDown += (sender, e) => { if (e.Buttons == (MouseButtons.Alternate)) { DoubleClick(sender, e, List2Position, FuncList2); } else { IndexSelected(sender, e, layout, Active, List2Position, FuncList2); } };
				Active.MouseMove += (sender, e) => MoveMoving(sender, e, layout, Active, Inactive, List2Position, List1Position, FuncList2, FuncList1);
				Active.MouseUp += (sender, e) => MoveEnded(sender, e, layout, Active, Inactive, List1Position, FuncList2, FuncList1);
				Active.MouseDoubleClick += (sender, e) => DoubleClick(sender, e, List2Position, FuncList2);
            }

            VisionMethods.OriginalImage = new Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte>(Condor.Properties.Resources.Condor1024x1024).Copy();
            LoadedImage.Image = VisionMethods.OpenCVImageToEto(Condor.Properties.Resources.Condor1024x1024);
            RunFinalMethods();

        }

    }
}
