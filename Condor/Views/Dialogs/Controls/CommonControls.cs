﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;


namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        private void CheckTextBoxTextInputIsInt(Object sender, TextChangingEventArgs e)
        {
            //Checks if a string from a textbox can be converted to an int 
            //e.Handled = !char.IsDigit(e.KeyChar) didn't work on my computer so have to make a work-a-round
            // Couldn't use the number row, only the number pad

            int n;
            if (!int.TryParse(e.Text, out n) && e.Text != "")
            {
                e.Cancel = true;
            }
        }
    }
}
