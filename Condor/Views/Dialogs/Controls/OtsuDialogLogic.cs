﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        void OnOtsuOKButtonClick(Dialog OtsuDialog, Slider Threshold, Slider MaxValue)
        {
            VisionMethods.OtsuThresholdValue = Threshold.Value;
            VisionMethods.OtsuMaxValue = MaxValue.Value;
            RunMethods();
            OtsuDialog.Close();
        }


        void OnOtsuMaxValueSliderChange(Slider OtsuSlider, TextBox OtsuTextBox)
        {
            FromSlider = true;
            OtsuTextBox.Text = OtsuSlider.Value.ToString();
            FromSlider = false;
        }

        void OnOtsuThresholdSliderChange(Slider OtsuSlider, TextBox OtsuTextBox)
        {
            FromSlider = true;
            OtsuTextBox.Text = OtsuSlider.Value.ToString();
            FromSlider = false;
        }


        private void OtsuTextBoxChange(TextBox OtsuTextBox, int Index, Slider ThresholdSlider, Slider MaxValueSlider, DefaultTableLayout settings)
        {
            Slider OtsuSlider;
            if(Index == 0)
            {
                OtsuSlider = MaxValueSlider;
            }
            else
            {
                OtsuSlider = ThresholdSlider;
            }


            if (OtsuTextBox.Text != "")
            {
                int value;
                int.TryParse(OtsuTextBox.Text, out value);
                if (value > 255)
                {
                    OtsuTextBox.Text = "255";
                }
                else if (value < 0)
                {
                    OtsuTextBox.Text = "0";
                }
                else
                {
                    OtsuSlider.Value = value;
                    OtsuTextBox.Text = value.ToString();
                    OtsuTextBox.Selection = new Range<int>(OtsuTextBox.Text.Length, OtsuTextBox.Text.Length);
                }
            }
			if (!FromSlider)
			{
                OtsuRun(settings, ThresholdSlider, MaxValueSlider);
			}

        }
        private void OtsuRun(DefaultTableLayout settings, Slider Threshold, Slider MaxValue)
        {
            int oldValueThreshold = VisionMethods.OtsuThresholdValue;
            int oldValueMax = VisionMethods.OtsuMaxValue;
			VisionMethods.OtsuThresholdValue = Threshold.Value;
			VisionMethods.OtsuMaxValue = MaxValue.Value;
			Image<Bgr, byte> Image = VisionMethods.EtoImageToOpenCV(new Bitmap(settings.BeforeImage.Image));
            VisionMethods.OtsuThreshold(ref Image);
			settings.AfterImage.Image = VisionMethods.OpenCVImageToEto(Image.ToBitmap());
            VisionMethods.OtsuThresholdValue = oldValueThreshold;
            VisionMethods.OtsuMaxValue = oldValueMax;
        }


    }
}
