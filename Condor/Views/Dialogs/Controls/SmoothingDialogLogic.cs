﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        bool FromSlider = false;
        private void OnSmoothingClose(Slider SmoothingSlider)
        {
            SmoothValue = SmoothingSlider.Value / (float)100.0;
        }

        private void SmoothSliderChange(Slider SmoothingSlider, TextBox SmoothingTextbox)
        {
            FromSlider = true;
            SmoothingTextbox.Text = SmoothingSlider.Value.ToString();
            SmoothValue = SmoothingSlider.Value / (float)100.0;
            FromSlider = false;
        }

        private void SmoothingTextBoxChange(TextBox SmoothingTextBox, Slider SmoothingSlider)
        {
            
            if (SmoothingTextBox.Text != "")
            {
                int value;
                int.TryParse(SmoothingTextBox.Text, out value);
                if (value > 100)
                {
                    SmoothingTextBox.Text = "100";
                }
                else if (value < 0)
                {
                    SmoothingTextBox.Text = "0";
                }
                else
                {
                    SmoothingSlider.Value = value;
                    SmoothingTextBox.Text = value.ToString();
                    SmoothingTextBox.Selection = new Range<int>(SmoothingTextBox.Text.Length, SmoothingTextBox.Text.Length);
                }
            }
            if(!FromSlider)
            {
                RunMethods();
            }
            
        }
    }
}
