﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Emgu.CV.Structure;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.Util;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        Image<Bgr, byte> finished(ref bool mouseDown, int mode, ref List<int> modes, ref List<List<System.Drawing.Point>> lines, 
                                  Image<Bgr, byte> image, List<System.Drawing.Point> markerPoints, ref VectorOfVectorOfPoint contours, double scale)
        {
            // checking for empty data.
            if (!modes.Any() && !markerPoints.Any())
            {
                return image;
            }
            mouseDown = false;
            if(markerPoints.Count != 0)
            {
                lines.Add(ScalePoints(scale, markerPoints));
                modes.Add(mode);
            }
           
            var cvPoints = lines.Select(x => x.ToArray()).ToArray();

            // do watershed with the drawn lines
            var watershed = watershedContours(image, ref contours, modes, cvPoints);

            
            return Redraw(image, contours, cvPoints, modes);
        }

        Image<Bgr, byte> watershedContours(Image<Bgr, byte> image, ref VectorOfVectorOfPoint contours, List<int> modes, System.Drawing.Point[][] cvPoints)
        {
            // do watershed with the drawn lines
            var watershed = VisionMethods.watershed(image, cvPoints, modes);

            CvInvoke.FindContours(watershed.Convert<Gray, byte>(), contours, null, RetrType.Tree, ChainApproxMethod.ChainApproxSimple);
            return watershed;
        }


        Image<Bgr, byte> Redraw(Image<Bgr, byte> image, VectorOfVectorOfPoint contours, System.Drawing.Point[][] cvPoints, List<int> modes)
        {
            CvInvoke.DrawContours(image, contours, -1, new Bgr(System.Drawing.Color.FromArgb(ContourColorWatershed.ToArgb())).MCvScalar, 2);

            
            for (int i = 0; i < cvPoints.Length; i++)
            {
                if (modes[i] == 1)
                    CvInvoke.Polylines(image, cvPoints[i], false, new Bgr(System.Drawing.Color.FromArgb(MaterialColorWatershed.ToArgb())).MCvScalar, 1);
                else
                    CvInvoke.Polylines(image, cvPoints[i], false, new Bgr(System.Drawing.Color.FromArgb(BackgroundColorWatershed.ToArgb())).MCvScalar, 1);
            }
            return image;
        }


        List<System.Drawing.Point> ScalePoints(double scale, List<System.Drawing.Point> points)
        {
            return points.Select(p => new System.Drawing.Point((int)Math.Round(p.X * scale), (int)Math.Round(p.Y * scale))).ToList();
        }

        Image<Bgr, byte> ScaleImage(Image<Bgr, byte> image, float height, float width, ref double scale)
        {
            var widthDif = width - image.Width;
            var heightDif =  height - image.Height;

            double returnScale = 1;


            if(widthDif <= heightDif)
            {
                scale = Math.Abs(width / image.Width);

                returnScale = image.Width / width;
            }
            else
            {
                scale = Math.Abs(height / image.Height);

                returnScale = image.Height / height;
            }

            try
            {
                image = image.Resize(scale, Inter.Area);
            }
            catch
            {
                return image;
            }
            scale = returnScale;
            return image;
        }
    }
}
