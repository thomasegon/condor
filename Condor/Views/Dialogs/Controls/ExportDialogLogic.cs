﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Condor.Views
{
	public partial class CondorForm : Form
	{
        Eto.Drawing.Image ShowOrigin(Object sender, MouseEventArgs e, ref List<Point> transformPoints, double scale)
        {
            Point point;
            var view = (ImageView)sender;
            Image<Bgr, byte> rhinoImage = VisionMethods.EtoImageToOpenCV(new Eto.Drawing.Bitmap(LoadedRhinoImage.Image));
            
            point = new Point((int)e.Location.X, (int)e.Location.Y);
            //CvInvoke.Circle(rhinoImage, point, 4, new Bgr(0, 0, 255).MCvScalar, -1);

            point = ScalePoint(scale, point);
            //CvInvoke.Circle(rhinoImage, point, 4, new Bgr(255, 0, 0).MCvScalar, -1);

            var flatContours = VisionMethods.Contours.ToArrayOfArray().SelectMany(array => array.Select(pt => pt)).ToArray();// should only look at external material.

            // need to scale points to fit image
            Point[] scaledPoints = flatContours; //ScalePoints(scale, new Point[][] { flatContours }, true)[0].ToArray();

            point = ClosestPoint(point, scaledPoints); // findes the closest contour point given the point. uses euclidian distance.

            // resets points and returns
            if (transformPoints.Count == 2)
            {
                transformPoints = new List<Point>();
                return VisionMethods.OpenCVImageToEto(rhinoImage.Bitmap);
            }
               

            // add selected points to list of points used to transform in rhino.
            if(transformPoints.Count == 0)
            {
                transformPoints.Add(point);
            }
            else if(transformPoints.Count == 1)
            {
                transformPoints.Add(point); 
            }

            // draws circles for the points
            if (transformPoints.Count > 0)
            {
                CvInvoke.Circle(rhinoImage, transformPoints[0], 4, new Bgr(0, 255, 0).MCvScalar, -1);
            }
            if (transformPoints.Count > 1)
            {
                CvInvoke.Circle(rhinoImage, transformPoints[1], 4, new Bgr(0, 255, 0).MCvScalar, -1);
            }
           
            return VisionMethods.OpenCVImageToEto(rhinoImage.Bitmap);
        }


        void FinalExportClick(Dialog ExportDialog, List<Point> transformPoints, int Distance)
        {
            transformPoints = transformPoints.OrderBy(p => p.X).ToList();
            OnExport(transformPoints, Distance);
            ExportDialog.Close();
            Close(); 
        }

        Point ClosestPoint(Point point, Point[] points)
        {
            Point closest = new Point();
            double minDistance = double.MaxValue;

            foreach (Point pt in points) 
            {
                double distance = EuclidianDistance(point, pt);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closest = pt;
                }
                
            }

            return closest;
        }

        double EuclidianDistance(Point pt1, Point pt2)
        {
            return Math.Sqrt(Math.Pow((pt1.X - pt2.X), 2) + Math.Pow((pt1.Y - pt2.Y), 2));
        }

        System.Drawing.Point ScalePoint(double scale, System.Drawing.Point point)
        {
            return new System.Drawing.Point((int)Math.Round(point.X * scale), (int)Math.Round(point.Y * scale));
        }

        // flips points vertically. uses middle og image height as a flip axis.
        VectorOfVectorOfPoint FlipVectorOfVectorPoints(VectorOfVectorOfPoint points)
        {
            var middle = VisionMethods.ResultImage.Height/2;
            Point[][] arrayofarray = points.ToArrayOfArray();
            for(int i = 0; i < arrayofarray.Length; i ++)
            {
                 arrayofarray[i] = arrayofarray[i].Select(pt => pt = new Point(pt.X, middle + middle - pt.Y)).ToArray();
            }
            return new VectorOfVectorOfPoint(arrayofarray);
        }

        // same as flipVectorOfVectorOfPoints but for VectorOFPoints
        List<Point> FlipListOfPoints(List<Point> points)
        {
            var middle = VisionMethods.ResultImage.Height / 2;
            Point[] array = points.ToArray();
            return new List<Point>(array.Select(pt => pt = new Point(pt.X, middle + middle - pt.Y)).ToArray());
        }
        
        void OnWidthChange(object sender, EventArgs e)
		{
            TextBox WidthTextBox = (TextBox)sender;
			int output;
			Int32.TryParse(WidthTextBox.Text, out output);
			MaterialWidth = output;
		}


		void OnDistanceChange(object sender, EventArgs e)
		{
            TextBox LengthTextBox = (TextBox)sender;
			int output;
			Int32.TryParse(LengthTextBox.Text, out output);
			MaterialLength = output;
		}

        void CheckForValidExport(Button FinalExportButton, TextBox RealDistanceTextBox, List<Point> MarkedPoints)
        {
            int n;
            bool DistanceIsInt = int.TryParse(RealDistanceTextBox.Text, out n) && RealDistanceTextBox.Text != "";

            FinalExportButton.Enabled = DistanceIsInt && MarkedPoints.Count == 2;
            
        }
    }
}
