﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
namespace Condor.Views
{
    public partial class CondorForm : Form
    {

        void OnBlurringOKButtonClick(Dialog BlurringDialog, Slider BlurringSlider)
        {
            VisionMethods.BlurringIterations = BlurringSlider.Value;
            RunMethods();
            BlurringDialog.Close();
        }

        private void BlurringSliderChange(Slider BlurringSlider, TextBox BlurringTextbox)
        {
            FromSlider = true;
            BlurringTextbox.Text = BlurringSlider.Value.ToString();
            FromSlider = false;
        }

        private void BlurringTextBoxChange(TextBox BlurringTextBox, Slider BlurringSlider, DefaultTableLayout settings)
        {

            if (BlurringTextBox.Text != "")
            {
                int value;
                int.TryParse(BlurringTextBox.Text, out value);
                if (value > 10)
                {
                    BlurringTextBox.Text = "10";
                }
                else if (value < 0)
                {
                    BlurringTextBox.Text = "0";
                }
                else
                {
                    BlurringSlider.Value = value;
                    BlurringTextBox.Text = value.ToString();
                    BlurringTextBox.Selection = new Range<int>(BlurringTextBox.Text.Length, BlurringTextBox.Text.Length);
                }
            }
			if (!FromSlider)
			{
                BlurringRun(settings,BlurringSlider);
			}

        }
		private void BlurringRun(DefaultTableLayout settings,Slider BlurringSlider)
		{
            int oldValue = VisionMethods.BlurringIterations;
            VisionMethods.BlurringIterations = BlurringSlider.Value;
			var result = VisionMethods.EtoImageToOpenCV(new Bitmap(settings.BeforeImage.Image));
            VisionMethods.Blurring(ref result);
			settings.AfterImage.Image = VisionMethods.OpenCVImageToEto(result.ToBitmap());
            VisionMethods.BlurringIterations = oldValue;
		}
    }
}
