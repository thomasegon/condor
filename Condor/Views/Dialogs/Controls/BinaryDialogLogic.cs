﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;


namespace Condor.Views
{
    public partial class CondorForm : Form
    {

        void SaveAndCloseBinaryOptions(Dialog SettingsDialog, Slider MaxValueSlider, Slider ThresholdSlider)
        {
            VisionMethods.BinaryMaxValue = MaxValueSlider.Value;
            VisionMethods.BinaryThresholdValue = ThresholdSlider.Value;
            RunMethods();
            SettingsDialog.Close();
        }

        void OnBinaryMaxValueSliderChange(Slider BinarySlider, TextBox BinaryTextBox)
        {
            FromSlider = true;
            BinaryTextBox.Text = BinarySlider.Value.ToString();
            FromSlider = false;
        }

        void OnBinaryThresholdSliderChange(Slider BinarySlider, TextBox BinaryTextBox)
        {
            FromSlider = true;
            BinaryTextBox.Text = BinarySlider.Value.ToString();
            FromSlider = false;
        }


        private void BinaryTextBoxChange(TextBox BinaryTextBox, int Index, Slider MaxValueSlider, Slider ThresholdSlider, DefaultTableLayout settings)
        {
            Slider BinarySlider;
            if(Index == 0)
            {
                BinarySlider = MaxValueSlider;
            }
            else
            {
                BinarySlider = ThresholdSlider;
            }
            if (BinaryTextBox.Text != "")
            {
                int value;
                int.TryParse(BinaryTextBox.Text, out value);
                if (value > 255)
                {
                    BinaryTextBox.Text = "255";
                }
                else if (value < 0)
                {
                    BinaryTextBox.Text = "0";
                }
                else
                {
                    BinarySlider.Value = value;
                    BinaryTextBox.Text = value.ToString();
                    BinaryTextBox.Selection = new Range<int>(BinaryTextBox.Text.Length, BinaryTextBox.Text.Length);
                }
            }
			if (!FromSlider)
			{
                BinaryRun(settings,MaxValueSlider,ThresholdSlider);
			}
        }
        private void BinaryRun(DefaultTableLayout settings, Slider MaxValueSlider, Slider ThresholdSlider)
		{
            int oldValueMax = VisionMethods.BinaryMaxValue;
            int oldValueThreshold = VisionMethods.BinaryThresholdValue;
			VisionMethods.BinaryMaxValue = MaxValueSlider.Value;
			VisionMethods.BinaryThresholdValue = ThresholdSlider.Value;
			var result = VisionMethods.EtoImageToOpenCV(new Bitmap(settings.BeforeImage.Image));
            VisionMethods.BinaryThreshold(ref result);
			settings.AfterImage.Image = VisionMethods.OpenCVImageToEto(result.ToBitmap());
            VisionMethods.BinaryMaxValue = oldValueMax;
            VisionMethods.BinaryThresholdValue = oldValueThreshold;
		}


    }
}
