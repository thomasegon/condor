﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Condor.Extensions;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;
using Eto.Forms;
using Eto.Drawing;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        void OnCroppingOKButtonClick(Dialog CroppingDialog, Slider CroppingSlider)
        {
            VisionMethods.CroppingMargin = CroppingSlider.Value; 
            RunMethods();
            CroppingDialog.Close();
        }


        private void CroppingSliderChange(Slider CroppingSlider, TextBox CroppingTextBox)
        {
            FromSlider = true;
            CroppingTextBox.Text = CroppingSlider.Value.ToString();
            FromSlider = false;
        }
        
        private void CroppingTextBoxChange(TextBox CroppingTextBox, Slider CroppingSlider,DefaultTableLayout settings)
        {

            if (CroppingTextBox.Text != "")
            {
                int value;
                int.TryParse(CroppingTextBox.Text, out value);
                if (value > CroppingSlider.MaxValue)
                {
                    CroppingTextBox.Text = CroppingSlider.MaxValue.ToString();
                }
                else if (value < CroppingSlider.MinValue)
                {
                    CroppingTextBox.Text = CroppingSlider.MinValue.ToString();
                }
                else
                {
                    CroppingSlider.Value = value;
                    CroppingTextBox.Text = value.ToString();
                    CroppingTextBox.Selection = new Range<int>(CroppingTextBox.Text.Length, CroppingTextBox.Text.Length);
                }
            }
			if (!FromSlider)
			{
                CroppingRun(settings, CroppingSlider);
			}

        }

        private int GetMaxMargin()
        {
            if(FuncList2.Contains(AllActions["Cropping"]))
                RunMethodsTo(FuncList2, Range);
            // find contours so we can crop to fit them
            var contours = new VectorOfVectorOfPoint();
            var hierachy = CvInvoke.FindContourTree(VisionMethods.ResultImage.Convert<Gray, byte>(), contours, ChainApproxMethod.ChainApproxSimple);
            var cropPoints = VisionMethods.FindMaterialContour(hierachy, contours);


            var rect = CvInvoke.BoundingRectangle(cropPoints);
            if (0 <= rect.X && 0 <= rect.Width && 0 <= rect.Y)
            {
                int MinHeightMargin = Math.Min(VisionMethods.ResultImage.Mat.Rows - (rect.Y + rect.Height), rect.Y);
                int MinWidthMargin = Math.Min(VisionMethods.ResultImage.Mat.Cols - (rect.X + rect.Width), rect.X);
                return Math.Min(MinHeightMargin, MinWidthMargin) - 1;
            }
            else { return 0; }
                
        }
		private void CroppingRun(DefaultTableLayout settings,Slider CroppingSlider)
		{
            int oldValue = VisionMethods.CroppingMargin;
            VisionMethods.CroppingMargin = CroppingSlider.Value;
            Image<Bgr, byte> Image = VisionMethods.EtoImageToOpenCV(new Bitmap(settings.BeforeImage.Image));
            VisionMethods.cropImage(ref Image);
            settings.AfterImage.Image = VisionMethods.OpenCVImageToEto(Image.ToBitmap());
            VisionMethods.CroppingMargin = oldValue;
		}
    }
}
