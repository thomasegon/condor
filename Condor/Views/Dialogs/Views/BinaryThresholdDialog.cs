﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        public Dialog BinaryDialog()
        {
            Label MaxValueLabel = new Label { Text = "Max Value" };
            Slider MaxValueSlider = new Slider { MinValue = 0, MaxValue = 255, SnapToTick = true, Width = 200, Value = VisionMethods.BinaryMaxValue };
            TextBox MaxValueTextBox = new TextBox { Text = VisionMethods.BinaryMaxValue.ToString() };

            Label ThresholdLabel = new Label { Text = "Threshold" };
            Slider ThresholdSlider = new Slider { MinValue = 0, MaxValue = 255, SnapToTick = true, Width = 200, Value = VisionMethods.BinaryThresholdValue };
            TextBox ThresholdTextBox = new TextBox { Text = VisionMethods.BinaryThresholdValue.ToString() };

            Dialog BinaryThresholdDialog = new Dialog();
            BinaryThresholdDialog.Title = "Binary Threshold Settings";

            Button SaveButton = new Button { Text = "Save" };
            Button CancelButton = new Button { Text = "Cancel"};


            #region Tooltips
            MaxValueSlider.ToolTip = "Set Max value";
			ThresholdSlider.ToolTip = "Set Threshold value";
            SaveButton.ToolTip = "Save changes";
			#endregion


			TableLayout SettingsLayout = new TableLayout
            {
                Rows =
                {
                    new TableLayout
                    {
                        Padding = new Eto.Drawing.Padding(10, 25),
                        Rows =
                        {
                            new TableRow
                            (
                                new TableCell(MaxValueLabel),
                                new TableCell(MaxValueSlider),
                                new TableCell(MaxValueTextBox)
                            ),
                            new TableRow
                            (
                                new TableCell(ThresholdLabel),
                                new TableCell(ThresholdSlider),
                                new TableCell(ThresholdTextBox)
                            )
                        }
                    },
                    new TableLayout
                    {
                        Padding = 10,
                        Rows =
                        {
                            null,
                            new TableRow
                            (
                                null,
                                new TableCell(CancelButton), 
                                new TableCell(SaveButton)
                            )
                        }
                    }
                        
                    
                }
            };
            if (FuncList2.Contains(AllActions["Binary Threshold"]))
            {
                RunMethodsTo(FuncList2, Range);
            }

            var beforeImage = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
            var afterImage = VisionMethods.ResultImage.Copy();
            VisionMethods.BinaryThreshold(ref afterImage);
            
            DefaultTableLayout settings = new DefaultTableLayout(SettingsLayout, beforeImage, VisionMethods.OpenCVImageToEto(afterImage.Bitmap));

			MaxValueSlider.ValueChanged += (sender, e) => OnBinaryMaxValueSliderChange((Slider)sender, MaxValueTextBox);
			MaxValueTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            MaxValueTextBox.TextChanged += (sender, e) => BinaryTextBoxChange((TextBox)sender, 0, MaxValueSlider, ThresholdSlider, settings);
            MaxValueSlider.MouseUp += (sender, e) => BinaryRun(settings, MaxValueSlider, ThresholdSlider);

			ThresholdSlider.ValueChanged += (sender, e) => OnBinaryThresholdSliderChange((Slider)sender, ThresholdTextBox);
			ThresholdTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            ThresholdTextBox.TextChanged += (sender, e) => BinaryTextBoxChange((TextBox)sender, 1, MaxValueSlider, ThresholdSlider, settings);
            ThresholdSlider.MouseUp += (sender, e) => BinaryRun(settings, MaxValueSlider, ThresholdSlider);

			SaveButton.Click += (sender, e) => SaveAndCloseBinaryOptions(BinaryThresholdDialog, MaxValueSlider, ThresholdSlider);
			CancelButton.Click += (sender, e) => BinaryThresholdDialog.Close();

            BinaryThresholdDialog.Content = settings;
            return BinaryThresholdDialog;
        }
    }
}
