﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Condor.Commands;
using Emgu.Util;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Condor.Views
{
	public partial class CondorForm : Form
	{
		public Dialog ExportDialog()
		{

            double scale = 1;

            var transformPoints = new List<System.Drawing.Point>();


            Dialog ExportDialog = new Dialog()
            {
                Maximizable = true,
                Minimizable = true,
                WindowState = WindowState.Maximized,
                Resizable = true,
                ShowInTaskbar = true
            };
			
			Button FinalExportButton = new Button { Text = "Export To Rhino" , Size = new Size(150, 25), Enabled = false};
            Label ExplanationLabel = new Label { Text = @"1) Select 2 points on the image that are supposed to be flat on the X-axis
2) Meassure the distance between the 2 points on the scrap material
3) Insert the meassured distance in the box below
4) Click Export, to export the finished contours to Rhino" };
			Label DistanceLabel = new Label { Text = "Distance between the 2 points in millimeters:" };
            Label ExportDetailLabel = new Label
            {
                Text = "Export Details",
                Size = new Size(200, 25),
                TextAlignment = TextAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

			TextBox DistanceTextBox = new TextBox
			{
				MaxLength = 10,
				Size = new Size(100, 25)
			};


            Image<Bgr, byte> rhinoImage = VisionMethods.EtoImageToOpenCV(new Bitmap(LoadedRhinoImage.Image));
            

            ImageView OriginView = new ImageView(); 
            TableLayout LeftLayout = new TableLayout
			{
				Rows =
				{
                    ExplanationLabel,
                    ExportDetailLabel,
                    new TableRow{
                        Cells = 
                        {
                            DistanceLabel
                        }
                    },
					new TableRow{
						Cells =
						{
                            DistanceTextBox
						}
					},
                    null,
                    new TableRow
                    {
                        Cells =
                        {
                            null, FinalExportButton
                        }
                    }
				}
			};

            TableLayout RightLayout = new TableLayout
            {
                Rows =
                {
                    OriginView, null
                }
            };

            

            


			TableLayout SettingsLayout = new TableLayout
			{
                Padding = 10,
                Rows =
				{
                    new TableRow{
                        Cells = 
                        {
                            LeftLayout, RightLayout, null
                        }
                    }
				}
			};


            OriginView.MouseDown += (sender, e) =>  { OriginView.Image = ShowOrigin(sender, e, ref transformPoints, scale); CheckForValidExport(FinalExportButton, DistanceTextBox, transformPoints); };

            DistanceTextBox.TextChanged += (sender, e) => { OnDistanceChange(sender, e); CheckForValidExport(FinalExportButton, DistanceTextBox, transformPoints); };
            DistanceTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            DistanceTextBox.KeyUp += (sender, e) => {
                if (FinalExportButton.Enabled)
                {
                    if (e.IsKeyDown(Keys.Enter))
                    {
                        FinalExportClick(ExportDialog, transformPoints, Convert.ToInt32(DistanceTextBox.Text));
                    }
                }
            };
            FinalExportButton.Click += (sender, e) => FinalExportClick(ExportDialog, transformPoints, Convert.ToInt32(DistanceTextBox.Text));

            ExportDialog.SizeChanged += (sender, e) =>
            {

                var size = new Size(Math.Max(SettingsLayout.Width - 400, 1), SettingsLayout.Height );
                rhinoImage = ScaleImage(VisionMethods.EtoImageToOpenCV(new Bitmap(LoadedRhinoImage.Image)), size.Height, size.Width, ref scale);
                
                
                OriginView.Image = VisionMethods.OpenCVImageToEto(rhinoImage.Bitmap);
                OriginView.Size = RightLayout.Size = OriginView.Image.Size;

            };

            ExportDialog.Content = SettingsLayout;

            return ExportDialog;
		}
	}
}
