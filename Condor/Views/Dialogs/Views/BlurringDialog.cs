﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        public Dialog BlurringDialog()
        {
            Dialog BlurringDialog = new Dialog();
            BlurringDialog.Title = "Blurring Settings";

            Label BlurringLabel = new Label { Text = "Blurring Iterations" };
            Slider BlurringSlider = new Slider { MinValue = 0, MaxValue = 10, Value = VisionMethods.BlurringIterations, Width = 200, SnapToTick = true };
            TextBox BlurringtextBox = new TextBox { Text = VisionMethods.BlurringIterations.ToString()};

            Button SaveButton = new Button { Text = "Save" };
            Button CancelButton = new Button { Text = "Cancel" };

			#region Tooltips
            BlurringSlider.ToolTip = "Set iterations of blurring";
            SaveButton.ToolTip = "Save changes";
            #endregion
            TableLayout SettingsLayout = new TableLayout
            {
                Rows =
                {
                    new TableLayout
                    {
                        Padding = new Eto.Drawing.Padding(10, 25),
                        Rows =
                        {
                            new TableRow
                            (
                                new TableCell(BlurringLabel),
                                new TableCell(BlurringSlider),
                                new TableCell(BlurringtextBox)
                            ),

                        }
                    },
                    new TableLayout
                    {
                        Padding = 10,
                        Rows =
                        {
                            null,
                            new TableRow
                            (
                                null,
                                new TableCell(CancelButton),
                                new TableCell(SaveButton)
                            )
                        }
                    }
                }
            };
			if (FuncList2.Contains(AllActions["Blurring"]))
			{
				RunMethodsTo(FuncList2, Range);
			}

            var beforeImage = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
            var afterImage = VisionMethods.ResultImage.Copy();
            VisionMethods.Blurring(ref afterImage);
            
            DefaultTableLayout setting = new DefaultTableLayout(SettingsLayout, beforeImage, VisionMethods.OpenCVImageToEto(afterImage.Bitmap));

			BlurringSlider.ValueChanged += (sender, e) => BlurringSliderChange(BlurringSlider, BlurringtextBox);
			BlurringtextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            BlurringtextBox.TextChanged += (sender, e) => BlurringTextBoxChange(BlurringtextBox, BlurringSlider, setting);
            BlurringSlider.MouseUp += (sender, e) => BlurringRun(setting, BlurringSlider);

			SaveButton.Click += (sender, e) => OnBlurringOKButtonClick(BlurringDialog, BlurringSlider);
			CancelButton.Click += (sender, e) => BlurringDialog.Close();

            BlurringDialog.Content = setting;
            return BlurringDialog;
        }



    }
}