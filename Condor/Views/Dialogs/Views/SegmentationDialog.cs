﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Condor.Views
{
    partial class CondorForm : Form
    {
        public Dialog SegmentationDialog()
        {
            Bgr color = new Bgr();
            var contours = new VectorOfVectorOfPoint();
            // stores drawn lines
            List<Point> markerPoints = new List<Point>();

            List<List<Point>> lines = new List<List<Point>>();
            List<int> modes = new List<int>();
            int mode = 1; // 1 for material (white) and 2 for background (black) used in watershed
            

            bool mouseDown = false;

            RunMethodsTo(FuncList2, Range); // makes sure we get the right version of the image
            
            Image<Bgr, byte> image = VisionMethods.ResultImage.Copy();

            // Scale down the image if it is to big.
            double scale = 1;

            var drawing = image.Copy();

            // if we already have some drawings from last time, we reuse them
            if (VisionMethods.WatershedMarkers != null && VisionMethods.WatershedMarkers.Any())
            {
                lines = VisionMethods.WatershedMarkers.Select(x => x.ToList()).ToList();
                modes = VisionMethods.WatershedModesList;
                drawing = finished(ref mouseDown, mode, ref modes, ref lines, image.Copy(), markerPoints, ref contours, scale);
            }


            #region Layout

            // used to show original image and drawings on it
            ImageView view = new ImageView() { Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap) }; 

            TableLayout DescriptionLayout = new TableLayout()
            {
                
                Rows =
                {
                    new Label() { Text = "This method uses the Watershed algorithm to segment the image into different parts. " +
                    "In this implementation we only distinguish between your material that you are interested in, and everything else is background. " +
                    "To do this you have to mark what is what by drawing on the image." },

                    new Label() { Text = "How to use: ", Font = new Eto.Drawing.Font(Eto.Drawing.SystemFont.Bold, 16) },
                    new Label() { Text = "Marking material: ", Font = new Eto.Drawing.Font(Eto.Drawing.SystemFont.Bold) },
                    new Label() { Text = "Left Click and start drawing" },
                    new Label(),
                    
                    new Label() { Text = "Marking background: ", Font = new Eto.Drawing.Font(Eto.Drawing.SystemFont.Bold) },
                    new Label() { Text = "Right click and start drawing. Alternatively use Ctrl+Left click on windows or Command+Left click on Mac"},
                    new Label(),

                    new Label() { Text = "Making straight lines: ", Font = new Eto.Drawing.Font(Eto.Drawing.SystemFont.Bold)},
                    new Label() { Text = "Holding down Shift while drawing either material or background makes a straight line between the start point and the mouse position. " +
                                         "This can be used for making clean cuts or making a countour more straight if it failed to do so automaticly. This can be done by making " +
                                         "both a straight material line on one side and a straight background line on the other side of where the contour line should be straight."},
                    new Label(),

                    new Label() { Text = "Removing mistakes:", Font = new Eto.Drawing.Font(Eto.Drawing.SystemFont.Bold)},
                    new Label() { Text = "In case of mistakes the reset button can be used to remove all markers. Ctrl+Z can be used to remove the last marker." }
                }
            };

            Button OKButton = new Button { Text = "OK", Width = 100, Height = 20 };
            Button CancelButton = new Button { Text = "Cancel", Width = 100, Height = 20 };
            Button ResetButton = new Button { Text = "Reset", Width = 100, Height = 20 };
            Label ColorContourLabel = new Label { Text = "Contour Color: ", TextAlignment = TextAlignment.Right };
            ColorPicker ColorPickerContour = new ColorPicker { Value = ContourColorWatershed, Width = 30 };

            Label ColorMaterialLabel = new Label { Text = "Material selection Color (left click): ", TextAlignment = TextAlignment.Right};
            ColorPicker ColorPickerMaterial = new ColorPicker { Value = MaterialColorWatershed };

            Label ColorBackgroundLabel = new Label { Text = "Background selection Color (right click): ", TextAlignment = TextAlignment.Right };
            ColorPicker ColorPickerBackground = new ColorPicker { Value = BackgroundColorWatershed };

			#region Tooltips
            ResetButton.ToolTip = "Reset";
            CancelButton.ToolTip = "Cancel changes";
			OKButton.ToolTip = "Save changes";
			#endregion

			TableLayout rightLayout = new TableLayout()
            {
                Padding = 10,
                Width = 400,
                Rows =
                {
                    new TableRow(DescriptionLayout),
                    null,
                    new TableLayout
                    {
                        Rows =
                        {
                            new TableRow(null, ColorContourLabel, ColorPickerContour),
                            new TableRow(null, ColorMaterialLabel, ColorPickerMaterial ),
                            new TableRow(null, ColorBackgroundLabel, ColorPickerBackground),
                        }
                    },
                    null,
                    new TableLayout
                    {
                        Rows =
                        {
                            new TableRow(null, ResetButton, CancelButton, OKButton, null)
                        }
                    }
                }

            };


            PixelLayout leftLayout = new PixelLayout();
            leftLayout.Add(view, 0, 0);

            TableLayout SettingsLayout = new TableLayout
            {
                Rows =
                {
                    new TableRow(
                        new TableCell(leftLayout), new TableCell(rightLayout))
                   
                }
            };
            Dialog SegmentationDialog = new Dialog() { Resizable = true, Maximizable = true, WindowState = WindowState.Maximized, Minimizable = true, ShowInTaskbar = true };
            SegmentationDialog.Title = "Object Selection Settings";
            SegmentationDialog.Content = SettingsLayout;
            SegmentationDialog.Size = SettingsLayout.Size;

            #endregion
            
            SegmentationDialog.SizeChanged += (sender, e) =>
            {
                var size = new Size(Math.Max(SettingsLayout.Width - 400, 1), SettingsLayout.Height);
                
                var rescaled = finished(ref mouseDown, mode, ref modes, ref lines, image.Copy(), markerPoints, ref contours, scale);
                drawing = ScaleImage(rescaled, size.Height, size.Width, ref scale);
                

                view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
                view.Size = view.Image.Size;
                leftLayout.Size = view.Image.Size;
            };
            
            // changes colors and redraws with the new color.
            ColorPickerMaterial.ValueChanged += (sender, e) =>
            {
                MaterialColorWatershed = ColorPickerMaterial.Value;
                drawing = Redraw(image.Copy(), contours, lines.Select(x => x.ToArray()).ToArray(), modes);
                view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
            };
            ColorPickerBackground.ValueChanged += (sender, e) =>
            {
                BackgroundColorWatershed = ColorPickerBackground.Value;
                drawing = Redraw(image.Copy(), contours, lines.Select(x => x.ToArray()).ToArray(), modes);
                view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
            };
            ColorPickerContour.ValueChanged += (sender, e) =>
            {
                ContourColorWatershed = ColorPickerContour.Value;
                drawing = Redraw(image.Copy(), contours, lines.Select(x => x.ToArray()).ToArray(), modes);
                view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
            };

            // resets all drawings.
            ResetButton.Click += (sender, e) =>
            {
                lines = new List<List<Point>>();
                markerPoints = new List<Point>();
                drawing = ScaleImage(image.Copy(), SettingsLayout.Height, SettingsLayout.Width - 400, ref scale);
                view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
                modes = new List<int>();
            };

            // Closes window and does not save 
            CancelButton.Click += (sender, e) => SegmentationDialog.Close();

            // saves changes and closes the window.
            OKButton.Click += (sender, e) =>
            {
                // scales the markers on load so they fit with scaled image.
                VisionMethods.WatershedMarkers = lines.Select(x => x.ToArray()).ToArray();
                VisionMethods.WatershedModesList = modes;

                RunMethods();
                SegmentationDialog.Close();
            };


            // control- z command. removes the last change and redo contours and drawing.
            SegmentationDialog.KeyDown += (sender, e) => 
            {
                if ((e.IsKeyDown(Keys.Z, Keys.Control) || e.IsKeyDown(Keys.Z, Keys.Application)) && lines.Count > 0)
                {
                    lines.RemoveAt(lines.Count - 1);
                    modes.RemoveAt(modes.Count - 1);

                    var rescaled = finished(ref mouseDown, mode, ref modes, ref lines, image.Copy(), markerPoints, ref contours, scale);
                    drawing = ScaleImage(rescaled, SettingsLayout.Height, SettingsLayout.Width - 400, ref scale);

                    view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
                }
            };

            // User is starting to draw.
            view.MouseDown += (sender, e) =>
                {
                    
                    if (e.Buttons == MouseButtons.Alternate || (e.Buttons == MouseButtons.Primary && e.Modifiers == Keys.Control || e.Modifiers == Keys.Application)) // user is drawing background
                    {
                        color = new Bgr(Color.FromArgb(BackgroundColorWatershed.ToArgb())); // red
                        mode = 2;
                    }
                    else if (e.Buttons == MouseButtons.Primary) // user is drawing material
                    {
                        color = new Bgr(Color.FromArgb(MaterialColorWatershed.ToArgb())); // green
                        mode = 1;
                    }
                    mouseDown = true;
                    markerPoints.Add(new Point((int)e.Location.X, (int)e.Location.Y)); // adds point to list
                    CvInvoke.Polylines(drawing, markerPoints.ToArray(), false, color.MCvScalar, 1);
                    view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
                };
            // user is drawing
            view.MouseMove += (sender, e) =>
            {
                var drawCopy = drawing.Copy();
                try
                {
                    if (mouseDown)
                    {
                        markerPoints.Add(new Point((int)e.Location.X, (int)e.Location.Y));
                        // for drawing straight lines between start point and current
                        if (e.Modifiers == Keys.Shift)
                        {
                            markerPoints = new List<Point>() { markerPoints[0], markerPoints[markerPoints.Count - 1] };
                        }

                        CvInvoke.Polylines(drawCopy, markerPoints.ToArray(), false, color.MCvScalar, 1);
                        view.Image = VisionMethods.OpenCVImageToEto(drawCopy.Bitmap);
                    }
                }
                catch (CvException) // mouse has gone out of bounds
                {
                    // do watershed with the drawn lines
                    var rescaled = finished(ref mouseDown, mode, ref modes, ref lines, image.Copy(), markerPoints, ref contours, scale);
                    drawing = ScaleImage(rescaled, SettingsLayout.Height, SettingsLayout.Width - 400, ref scale);
                    markerPoints = new List<Point>();
                    // show image with drawing on
                    view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
                }
            };

            // when mouse is lifted we want to do watershed to show the result. same as the catch above
            view.MouseUp += (sender, e) =>
            {
                // do watershed with the drawn lines
                var rescaled = finished(ref mouseDown, mode, ref modes, ref lines, image.Copy(), markerPoints, ref contours, scale);
                drawing = ScaleImage(rescaled, SettingsLayout.Height, SettingsLayout.Width - 400, ref scale);
                markerPoints = new List<Point>();
                // show image with drawing on
                view.Image = VisionMethods.OpenCVImageToEto(drawing.Bitmap);
            };

            return SegmentationDialog;
        }
    }
}
