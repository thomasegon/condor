﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        public Dialog OtsuDialog()
        {



            Label MaxValueLabel = new Label { Text = "Max Value" };
            Slider MaxValueSlider = new Slider { MinValue = 0, MaxValue = 255, SnapToTick = true, Width = 200, Value = VisionMethods.OtsuMaxValue };
            TextBox MaxValueTextBox = new TextBox { Text =  VisionMethods.OtsuMaxValue.ToString() };

            Label ThresholdLabel = new Label { Text = "Threshold" };
            Slider ThresholdSlider = new Slider { MinValue = 0, MaxValue = 255, SnapToTick = true, Width = 200, Value = VisionMethods.OtsuThresholdValue};
            TextBox ThresholdTextBox = new TextBox { Text = VisionMethods.OtsuThresholdValue.ToString() };

            Dialog OtsuThresholdDialog = new Dialog() { Resizable = true };
            OtsuThresholdDialog.Title = "Boundary Enchancement Settings";

            Button SaveButton = new Button { Text = "Save" };
            Button CancelButton = new Button { Text = "Cancel" };


            #region Tooltips
            MaxValueSlider.ToolTip = "Set Max value";
            ThresholdSlider.ToolTip = "Set Threshold value";
            SaveButton.ToolTip = "Save changes";
            #endregion



            TableLayout SettingsLayout = new TableLayout
            {
                Rows =
                {
                    new TableLayout
                    {
                        Padding = new Eto.Drawing.Padding(10, 25),
                        Rows =
                        {
                            new TableRow
                            (
                                new TableCell(MaxValueLabel),
                                new TableCell(MaxValueSlider),
                                new TableCell(MaxValueTextBox)
                            ),
                            new TableRow
                            (
                                new TableCell(ThresholdLabel),
                                new TableCell(ThresholdSlider),
                                new TableCell(ThresholdTextBox)
                            )
                        }
                    },
                    new TableLayout
                    {
                        Padding = 10,
                        Rows =
                        {
                            null,
                            new TableRow
                            (   
                                null,
                                new TableCell(CancelButton),
                                new TableCell(SaveButton)
                            )
                        }
                    } 
                }
            };
			if (FuncList2.Contains(AllActions["Boundary Enchancement"]))
			{
				RunMethodsTo(FuncList2, Range);
			}

			var beforeImage = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
			Image<Bgr, byte> Image = VisionMethods.EtoImageToOpenCV(new Bitmap(beforeImage));
			VisionMethods.OtsuThreshold(ref Image);
			var afterImage = VisionMethods.OpenCVImageToEto(Image.ToBitmap());

            DefaultTableLayout settings = new DefaultTableLayout(SettingsLayout, beforeImage, afterImage);

			MaxValueSlider.ValueChanged += (sender, e) => OnOtsuMaxValueSliderChange((Slider)sender, MaxValueTextBox);
			MaxValueTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            MaxValueTextBox.TextChanged += (sender, e) => OtsuTextBoxChange((TextBox)sender, 0, ThresholdSlider, MaxValueSlider, settings);
            MaxValueSlider.MouseUp += (sender, e) => OtsuRun(settings, ThresholdSlider, MaxValueSlider);

			ThresholdSlider.ValueChanged += (sender, e) => OnOtsuThresholdSliderChange((Slider)sender, ThresholdTextBox);
			ThresholdTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            ThresholdTextBox.TextChanged += (sender, e) => OtsuTextBoxChange((TextBox)sender, 1, ThresholdSlider, MaxValueSlider, settings);
            ThresholdSlider.MouseUp += (sender, e) => OtsuRun(settings, ThresholdSlider, MaxValueSlider);

			SaveButton.Click += (sender, e) => OnOtsuOKButtonClick(OtsuThresholdDialog, ThresholdSlider, MaxValueSlider);
			CancelButton.Click += (sender, e) => OtsuThresholdDialog.Close();

            OtsuThresholdDialog.Content = settings;
            return OtsuThresholdDialog;
        }
    }
}
