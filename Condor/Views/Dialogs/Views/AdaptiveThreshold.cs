﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;


namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        public Dialog AdaptiveThresholdDialog()
        {
            Label MaxValueLabel = new Label { Text = "Max Value" };
            Slider MaxValueSlider = new Slider { };

            Label BlockSizeLabel = new Label { Text = "Block Size" };
            TextBox BlockSizeTextbox = new TextBox { };

            Label Param1Label = new Label { Text = "Param1" };
            Slider Param1Slider = new Slider { };

            Dialog AdaptiveThresholdDialog = new Dialog();
            AdaptiveThresholdDialog.Title = "Adaptive Threshold Settings";

            Button OKButton = new Button { Text = "OK"};


            TableLayout SettingsLayout = new TableLayout
            {   
                Rows =
                {
                    new TableRow
                    (
                        new TableCell(MaxValueLabel),
                        new TableCell(MaxValueSlider)

                    ),
                    new TableRow
                    (
                        new TableCell(BlockSizeLabel),
                        new TableCell(BlockSizeTextbox)
                    ),
                    new TableRow
                    (
                        new TableCell(Param1Label),
                        new TableCell(Param1Slider)
                    ),
                    new TableRow
                    (
                        new TableCell(OKButton)

                    )
                }
            };



            AdaptiveThresholdDialog.Content = SettingsLayout;

            //AdaptiveThresholdDialog.Closed += (sender, e) => OnDialogClose(AdaptiveThresholdDialog);
            return AdaptiveThresholdDialog;
        }

        private void CancelButton_Click(object sender, EventArgs e, Dialog test)
        {
            test.Close();
        }
    }
}
