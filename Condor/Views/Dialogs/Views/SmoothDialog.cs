﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        public Dialog SmoothDialog ()
        {
            Dialog SmoothDialog = new Dialog();
            SmoothDialog.Title = "Smoothing Settings";

            Label SmoothingLabel = new Label { Text = "Smoothing" };
            Slider SmoothingSlider = new Slider { MinValue=0, MaxValue=100, Value = (int)(SmoothValue*100), Width=200, SnapToTick=true};
            TextBox SmoothingTextBox = new TextBox { Text = ((int)(SmoothValue*100)).ToString() };

            Button OKButton = new Button { Text = "OK" };

            SmoothingSlider.ValueChanged += (sender, e) => SmoothSliderChange(SmoothingSlider, SmoothingTextBox);
            SmoothingTextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            SmoothingTextBox.TextChanged += (sender, e) => SmoothingTextBoxChange(SmoothingTextBox, SmoothingSlider);
            SmoothDialog.Closed += (sender, e) => OnSmoothingClose(SmoothingSlider);

            TableLayout SettingsLayout = new TableLayout
            {
                Rows =
                {

                    new TableRow
                    (
                        new TableCell(SmoothingLabel),
                        new TableCell(SmoothingSlider),
                        new TableCell(SmoothingTextBox)
                    ),
                    new TableRow
                    (
                        new TableCell(OKButton)
                    )
                }
            };



            SmoothDialog.Content = SettingsLayout;
            return SmoothDialog;
        }

        

    }
}