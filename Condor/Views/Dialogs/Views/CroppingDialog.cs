﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV;

namespace Condor.Views
{
    public partial class CondorForm : Form
    {
        public Dialog CroppingDialog()
        {
            Dialog CroppingDialog = new Dialog();
            CroppingDialog.Title = "Cropping Settings";

            Label CroppingLabel = new Label { Text = "Margin" };
            Slider CroppingSlider = new Slider { MinValue = 0, MaxValue = GetMaxMargin(), Value = VisionMethods.CroppingMargin, Width = 200, SnapToTick = true };
            TextBox CroppingtextBox = new TextBox { Text = VisionMethods.CroppingMargin.ToString() };

            Button SaveButton = new Button { Text = "Save" };
            Button CancelButton = new Button { Text = "Cancel" };

			#region Tooltips
            CroppingSlider.ToolTip = "Set Margin size";
            SaveButton.ToolTip = "Save changes";
            #endregion

            TableLayout SettingsLayout = new TableLayout
            {
                Rows =
                {
                    new TableLayout
                    {
                        Padding = new Eto.Drawing.Padding(10, 25),
                        Rows =
                        {
                            new TableRow
                            (
                                new TableCell(CroppingLabel),
                                new TableCell(CroppingSlider),
                                new TableCell(CroppingtextBox)
                            )
                        }
                    },
                    null,
                    new TableLayout
                    {
                        Padding = 10,
                        Rows =
                        {
                            new TableRow
                            (   
                                null,
                                new TableCell(CancelButton),
                                new TableCell(SaveButton)
                            )
                        }
                    }
                }
            };
			if (FuncList2.Contains(AllActions["Cropping"]))
			{
				RunMethodsTo(FuncList2, Range);
			}
			

            var beforeImage = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
			Image<Bgr, byte> Image = VisionMethods.EtoImageToOpenCV(new Bitmap(beforeImage));
            VisionMethods.cropImage(ref Image);
            var afterImage = VisionMethods.OpenCVImageToEto(Image.ToBitmap());

            DefaultTableLayout settings = new DefaultTableLayout(SettingsLayout, beforeImage, afterImage);

			CroppingSlider.ValueChanged += (sender, e) => CroppingSliderChange(CroppingSlider, CroppingtextBox);
			CroppingtextBox.TextChanging += (sender, e) => CheckTextBoxTextInputIsInt(sender, e);
            CroppingtextBox.TextChanged += (sender, e) => CroppingTextBoxChange(CroppingtextBox, CroppingSlider, settings);
            CroppingSlider.MouseUp += (sender, e) => CroppingRun(settings, CroppingSlider);

			SaveButton.Click += (sender, e) => OnCroppingOKButtonClick(CroppingDialog, CroppingSlider);
			CancelButton.Click += (sender, e) => CroppingDialog.Close();

            CroppingDialog.Content = settings;
            return CroppingDialog;
        }



    }
}