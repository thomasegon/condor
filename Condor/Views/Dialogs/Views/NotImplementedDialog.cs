﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;

namespace Condor.Views
{
    partial class CondorForm : Form
    {
        public Dialog NotImplemetedDialog()
        {

            var NotImplementedDialog = new Dialog() { Resizable = true};

            var message = new Label { Text = "Sorry, this setting has not been implemented yet." };
            var CloseButtion = new Button { Text = "Close", Width = 100, Height = 20 };

            var table = new TableLayout()
            {
                Rows =
                {
                    null,
                    new TableRow
                    (
                        null,
                        new TableCell(message),
                        null
                    ),
                    new TableRow
                    (
                        null,
                        new TableCell(CloseButtion),
                        null
                    ),
                    null
                },
                Padding = 20

            };

            
            CloseButtion.Click += (sender, e) => NotImplementedDialog.Close();

            NotImplementedDialog.Content = table;
            return NotImplementedDialog;
        }
    }
}
                
                
