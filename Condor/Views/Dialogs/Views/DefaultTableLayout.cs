﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;

namespace Condor.Views
{
    class DefaultTableLayout : TableLayout
    {
        TableLayout LeftLayout;
        TableLayout RightLayout;
        public ImageView BeforeImage;
        public ImageView AfterImage;

        public DefaultTableLayout(TableLayout left, Image before, Image after)
        {

            LeftLayout = left;

            Label beforeLabel = new Label() { Text = "Before image" };
            BeforeImage = new ImageView() { Image = before, Size = new Size(400, 400), ToolTip = "This is the image as it is right before appying this method. How it looks depends on the methods before it."};

            Label afterLabel = new Label() { Text = "After image" };
            AfterImage = new ImageView() { Image = after, Size = new Size(400, 400), ToolTip = "This is the resulting image after applying the method to the before image." };

            TableLayout beforeLayout = new TableLayout()
            {
                Padding = 10,
                Rows =
                {
                    beforeLabel,
                    BeforeImage
                }

            };

            TableLayout afterLayout = new TableLayout()
            {
                Padding = 10,
                Rows =
                {
                    afterLabel,
                    AfterImage
                }

            };
            

            RightLayout = new TableLayout()
            {
                Rows =
                {
                    new TableRow(beforeLayout,  afterLayout, null),
                    null
                }
            };

            Rows.Add(new TableRow(LeftLayout, RightLayout));
        }
    }
}
