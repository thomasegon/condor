﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Condor.ComputerVision;

namespace Condor.Views
{
    partial class CondorForm
    {
        // runs all methods in list
        // [0, last]
        void RunMethods()
        {
            switch (Tabs.SelectedIndex)
                {
                    case 0:
                        RunBasicMethods();
                        break;
                    case 1:
                        RunAdvancedMethods();
                        break;
                }
        }
        void RunAdvancedMethods()
        {
            var methods = FuncList2;
            if (VisionMethods.ResultImage != null)
            {
                VisionMethods.ResultImage = VisionMethods.OriginalImage.Copy();
                foreach (FuncItem func in methods)
                    func.Run();

                RunFinalMethods();

            }
        }
        // runs from index to the last
        // [index, last]
        void RunMethodsFrom(List<FuncItem> methods, int index)
        {
            for (int i = index; i < methods.Count; i++)
                methods[i].Run();

            RunFinalMethods();
        }
        // runs methods in list from start to index exclusive
        // [0, index[
        void RunMethodsTo(List<FuncItem> methods, int index)
        {
            VisionMethods.ResultImage = VisionMethods.OriginalImage.Copy();
            for (int i = 0; i < index; i++)
                methods[i].Run();
        }
        void RunBasicMethods()
        {
            VisionMethods.ResultImage = VisionMethods.OriginalImage.Copy();
            VisionMethods.Blurring(); 
            VisionMethods.OtsuThreshold();
            VisionMethods.BinaryThreshold();
            if ((bool)HomographyCheckBox.Checked)
            {
                VisionMethods.Homography();
            }
            RunFinalMethods();
        }

        void RunFinalMethods()
        {
            /*
            VisionMethods.FindContours();
            try
            {
                //VisionMethods.ImageRotation();
            }
            catch
            {
                MessageBox.Show("Oh no! We could not find proper contours :(");
                return;
            }
            */
            VisionMethods.FindContours();
            LoadedImage.Image = VisionMethods.OpenCVImageToEto(VisionMethods.ResultImage.Bitmap);
            LoadedRhinoImage.Image = VisionMethods.OpenCVImageToEto(VisionMethods.MakeRhinoView(SmoothValue).Bitmap);

        }

        void DefaultMethodSetup(out List<FuncItem> active, out List<FuncItem> inactive)
        {
            active = new List<FuncItem>()
            {
                AllActions["Blurring"],
                AllActions["Boundary Enchancement"]
            };
			inactive = new List<FuncItem>();
			foreach (var item in AllActions)
			{
				if (!active.Contains(item.Value))
				{
					inactive.Add(item.Value);
				}
			}
        }

        // idea for a class, but types would have to be changed and all list methods added.
        class methodList
        {
            public List<FuncItem> methods;

            public void Run()
            {
                foreach (FuncItem method in methods)
                {
                    method.Run();
                }
            }

            public void Add(FuncItem item)
            {
                methods.Add(item);
            }

            public void Remove(FuncItem item)
            {
                methods.Remove(item);
            }

            public methodList()
            {
                methods = new List<FuncItem>();
            }
        }
        //Class used to store a name and a preset list
        public class PresetItem
        {
			string _name;
			private List<FuncItem> _list;
            public List<FuncItem> PresetList
            {
                get { return _list; }
            }
			public override string ToString()
			{
				return this._name;
			}
            public PresetItem(string name, List<FuncItem> l)
            {
                this._name = name;
                this._list = l;
            }
            public List<List<FuncItem>> Run(Dictionary<string, FuncItem> AllActions)
            {
                List<List<FuncItem>> Result = new List<List<FuncItem>>();
                List<FuncItem> ActiveList = new List<FuncItem>();
                foreach (var item in PresetList)
                {
                    ActiveList.Add(item);
                }
                List<FuncItem> InactiveList = new List<FuncItem>();
                foreach(var item in AllActions)
                {
                    if(!_list.Contains(item.Value))
                    {
                        InactiveList.Add(item.Value);
                    }
                }; 
                Result.Add(InactiveList);
                Result.Add(ActiveList);
                return Result;
            }

        }
        // class used to store a name, function and settings
        public class FuncItem
        {
            string _name;
            Action _func;
            string _tooltip;
            Func<Dialog> _settingsdialog;

            public override string ToString()
            {
                return this._name;
            }
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }


            public Action Func
            {
                get
                {
                    return _func;
                }
                set
                {
                    _func = value;
                }
            }
            public string Tooltip
            {
                get { return _tooltip; }
            }
            public Func<Dialog> SettingsDialog
            {
                get { return _settingsdialog; }
            }

            public FuncItem(string name, Action func, Func<Dialog> SettingsDialog, string Tooltip = "")
            {
                this.Name = name;
                this.Func = func;
                this._tooltip = Tooltip;
                this._settingsdialog = SettingsDialog;
            }

            public Dialog RetrieveDialog()
            {
                //Runs the function that generates a dialog for the given Action
                return SettingsDialog();
            }


            public void Run()
            {
                _func();
            }
        }
    }
}
