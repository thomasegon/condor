﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Rhino.Geometry;
using Emgu.CV.Util;

namespace Condor.Extensions
{
    public static class CondorExtensions
    {
        public static T[] GetRow<T>(this T[,] array, int row)
        {
            if (!typeof(T).IsPrimitive)
                throw new InvalidOperationException("Not supported for managed types.");

            if (array == null)
                throw new ArgumentNullException("array");

            int cols = array.GetUpperBound(1) + 1;
            T[] result = new T[cols];
            int size = Marshal.SizeOf(typeof(T));

            Buffer.BlockCopy(array, row * cols * size, result, 0, cols * size);

            return result;
        }

        public static List<Point3d> ToRhinoPoints(this List<System.Drawing.Point> DrawingPoints)
        {
            List<Point3d> Result = new List<Point3d>();
            foreach (System.Drawing.Point DrawingPoint in DrawingPoints)
            {
                Result.Add(new Point3d(DrawingPoint.X, DrawingPoint.Y, 0.0));
            }
            return Result;
        }

        public static List<Point3d> ToRhinoPoints(this VectorOfPoint Vector, bool closed=false)
        {
            List<Point3d> Result = new List<Point3d>();
            for (int i = 0; i < Vector.Size; i++)
            {
                Result.Add(new Point3d(Vector[i].X, Vector[i].Y, 0.0));
            }
            if (closed)
            {
                Result.Add(new Point3d(Vector[0].X, Vector[0].Y, 0.0));
            }
            return Result;
        }

        public static List<List<Point3d>> ToRhinoPoints(this VectorOfVectorOfPoint VectorOfVector, bool closed=false)
        {
            List<List<Point3d>> Result = new List<List<Point3d>>();
            for (int i = 0; i < VectorOfVector.Size; i++)
            {
                Result.Add(VectorOfVector[i].ToRhinoPoints(closed));
            }
            return Result;
        }

        public static List<System.Drawing.Point> ToDrawingPoints(this VectorOfPoint Vector)
        {
            List<System.Drawing.Point> Result = new List<System.Drawing.Point>();
            for (int i = 0; i < Vector.Size; i++)
            {
                Result.Add(new System.Drawing.Point(Vector[i].X, Vector[i].Y));
            }
            return Result;
        }


        public static List<List<System.Drawing.Point>> ToDrawingPoints(this VectorOfVectorOfPoint VectorOfVector)
        {
            List<List<System.Drawing.Point>> Result = new List<List<System.Drawing.Point>>();
            for (int i = 0; i < VectorOfVector.Size; i++)
            {
                Result.Add(VectorOfVector[i].ToDrawingPoints());
            }
            return Result;
        }

        public static List<System.Drawing.Point> ToDrawingPoints(this List<Point3d> RhinoPoints)
        {
            List<System.Drawing.Point> Result = new List<System.Drawing.Point>();
            foreach (Point3d RhinoPoint in RhinoPoints)
            {
                Result.Add(new System.Drawing.Point((int)RhinoPoint.X, (int)RhinoPoint.Y));
            }
            return Result;
        }

        public static VectorOfPoint ToVectorOfPoint(this List<Point3d> RhinoPoints)
        {
            System.Drawing.Point[] InputArray = RhinoPoints.ToDrawingPoints().ToArray();
            VectorOfPoint Result = new VectorOfPoint();
            Result.Push(InputArray);
            return Result;
        }

        public static VectorOfVectorOfPoint ToVectorOfVectorofPoint(this List<List<Point3d>> RhinoPointLists)
        {
            VectorOfVectorOfPoint Result = new VectorOfVectorOfPoint();
            foreach (List<Point3d> RhinoList in RhinoPointLists)
            {
                Result.Push(RhinoList.ToVectorOfPoint());
            }
            return Result;
        }


    }
}
