[TOC]


# Install Condor from PluginManager

## Windows
Download plugin.
```
https://bitbucket.org/thomasegon/condor/downloads/Condor.rhi
```

To install Condor plug-in for Windows should be very simple. 

1. Double click the Condor.rhi file.
2. Select if you want to install it for yourself or every user on the PC, and then click next.
3. Wait until the Installer is done and click the close button.

## OS X
Download plugin without Mono.
```
https://bitbucket.org/thomasegon/condor/downloads/Condor.app
```
To install Condor plug-in for OS X should be very simple. 

1. Double click the Condor.app file.
2. Wait until the Installer is done.

Download plugin with Mono
```
https://bitbucket.org/thomasegon/condor/downloads/CondorMono.app
```
To install Condor plug-in for OS X should be very simple. 

1. Double click the CondorMono.app file.
2. Follow the instructions.
3. Wait until the Installer is done.

# Install Condor from git
```
git clone https://thomasegon@bitbucket.org/thomasegon/p9.git
```

## Windows
Copy all files from Condor/Assembly_Windows/ to Condor/bin/Debug/

```
copy Condor/Assembly_Windows/* Condor/bin/Debug/
```

Then build project.

Exception settings Visual Studio:
Disable disconnect 
Disable deadlock

## OS X 
For Condor to work it needs assembly from EmguCV.
```
cp Condor/Assembly_OSX/* Condor/bin/Debug/
```

Then build project.

You need to remove an old assembly from Rhino.
### Version 5:

Either remove or rename one of Rhino's System.Drawing assembly, because it is a old version and Condor conflicts with this old assembly.
```
sudo move /Applications/Rhinoceros.app/Contents/Resources/lib/mono/gac/System.Drawing/4.0.0.0__b03f5f7f11d50a3a/System.Drawing /Applications/Rhinoceros.app/Contents/Resources/lib/mono/gac/System.Drawing/4.0.0.0__b03f5f7f11d50a3a/back.dll
```

When removing the old assembly from Rhino the newer one is missing libgdiplus.dylib. Replace X.X.X with the version of Mono you have installed. 
Use ```mono -V``` to find the Mono version and take only the first three numbers. 

```
cp /Library/Frameworks/Mono.framework/Versions/X.X.X/lib/libgdiplus.dylib /Applications/Rhinoceros.app/Contents/Frameworks/Mono64Rhino.framework/Versions/4.0.2/lib/
```

### Version WIP:
Either remove or rename one of Rhino's System.Drawing assembly, because it is a old version and Condor conflicts with this old assembly.
```
sudo move /Applications/RhinoWIP.app/Contents/Resources/lib/mono/gac/System.Drawing/4.0.0.0__b03f5f7f11d50a3a/System.Drawing /Applications/RhinoWIP.app/Contents/Resources/lib/mono/gac/System.Drawing/4.0.0.0__b03f5f7f11d50a3a/back.dll
```

When removing the old assembly from Rhino the newer one is missing libgdiplus.dylib. Replace X.X.X with the version of Mono you have installed. 
Use ```mono -V``` to find the Mono version and take only the first three numbers. 

```
cp /Library/Frameworks/Mono.framework/Versions/X.X.X/lib/libgdiplus.dylib /Applications/RhinoWIP.app/Contents/Frameworks/Mono64Rhino.framework/Versions/4.0.2/lib/
```